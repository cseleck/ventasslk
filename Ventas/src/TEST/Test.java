package TEST;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	int diff = minutesDiff(GetItemDate("11/21/2011 7:00:00 AM"), GetItemDate("11/21/2011 1:00:00 PM"));

	public static Date GetItemDate(final String date)
	{
	    final Calendar cal = Calendar.getInstance(TimeZone.getDefault());
	    final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
	    format.setCalendar(cal);

	    try {
	        return format.parse(date);
	    } catch (Exception e) {
	        return null;
	    }
	}

	public static int minutesDiff(Date earlierDate, Date laterDate)
	{
	    if( earlierDate == null || laterDate == null ) return 0;

	    return (int)((laterDate.getTime()/60000) - (earlierDate.getTime()/60000));
	}

}
