package com.mx.totalplay.dao.ws;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.totalplay.dao.UsuariosDAO;
import com.mx.totalplay.utils.Constantes;
import com.sforce.soap.partner.DeleteResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.SearchRecord;
import com.sforce.soap.partner.SearchResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectorConfig;

@Component
public class ClientSFSOAWSDAO {
	private final Logger logger = LogManager.getLogger();
	
	@Autowired 
	private UsuariosDAO usuariosDAO;
	
	public QueryResult query(String queryString) {
		logger.info("***** UtileriasSfWSDAO > query()");
		QueryResult Quey = new QueryResult();
		try {
			String SessionIdValid = usuariosDAO.getSessionIDSF();
			ConnectorConfig config = new ConnectorConfig();
			config.setUsername(Constantes.USERSF);
			config.setPassword(Constantes.PASSWD_SF);
			config.setServiceEndpoint(Constantes.LOGINSF);
			config.setSessionId(SessionIdValid);
			PartnerConnection partnerConnection ;
			partnerConnection = new PartnerConnection(config);
			logger.info("***** Client_ServiceSOA_APISF_WSDAO > query"+ "\n queryString === " + queryString);
			Quey = partnerConnection.query(queryString);
		} catch (Exception e) {			
			logger.error("C A T C H        ***** UtileriasSfWSDAO > query()");
			logger.error(e);
			e.printStackTrace();
		}
		return Quey;
	}
	
	public SaveResult[] update(SObject[] sObjects) {
		SaveResult[] RUpdate = new SaveResult[0];
		try {
			String SessionIdValid = usuariosDAO.getSessionIDSF();
			ConnectorConfig config = new ConnectorConfig();
			config.setUsername(Constantes.USERSF);
			config.setPassword(Constantes.PASSWD_SF);
			config.setServiceEndpoint(Constantes.LOGINSF);
			config.setSessionId(SessionIdValid);
		PartnerConnection partnerConnection;
		partnerConnection = new PartnerConnection(config);
		logger.info("***** Client_ServiceSOA_APISF_WSDAO > update"+ "\n sObjects.length === " + sObjects.length);
		RUpdate = partnerConnection.update(sObjects);
		} catch (Exception e) {			
			logger.error("C A T C H        ***** UtileriasSfWSDAO > query()");
			logger.error(e);
			e.printStackTrace();
		}
		return RUpdate;
	}
	
	public SaveResult[] create(SObject[] sObjects) {
		SaveResult[] createR = new SaveResult[0];
		try {
			String SessionIdValid = usuariosDAO.getSessionIDSF();
			ConnectorConfig config = new ConnectorConfig();
			config.setUsername(Constantes.USERSF);
			config.setPassword(Constantes.PASSWD_SF);
			config.setServiceEndpoint(Constantes.LOGINSF);
			config.setSessionId(SessionIdValid);
			PartnerConnection partnerConnection;
			partnerConnection = new PartnerConnection(config);
			logger.info("***** Client_ServiceSOA_APISF_WSDAO > create" + "\n sObjects.length === " + sObjects.length);
			createR = partnerConnection.create(sObjects);
		} catch (Exception e) {
			logger.error("C A T C H        ***** UtileriasSfWSDAO > create()");
			logger.error(e);
			e.printStackTrace();
		}
		return createR;
	}
	
	public DeleteResult[] delete(String [] ids) {
		DeleteResult[] deleteResults = new DeleteResult [0];
		try {
			String SessionIdValid = usuariosDAO.getSessionIDSF();
			ConnectorConfig config = new ConnectorConfig();
			config.setUsername(Constantes.USERSF);
			config.setPassword(Constantes.PASSWD_SF);
			config.setServiceEndpoint(Constantes.LOGINSF);
			config.setSessionId(SessionIdValid);
			PartnerConnection partnerConnection ;
			partnerConnection = new PartnerConnection(config);
			logger.info("***** Client_ServiceSOA_APISF_WSDAO > delete" + "\n ids.length === " + ids.length);
			deleteResults = partnerConnection.delete(ids);
		} catch (Exception e) {			
			logger.error("C A T C H        ***** UtileriasSfWSDAO > delete()");
			logger.error(e);
			e.printStackTrace();
		}
		return deleteResults;
	}
	
	public SearchRecord[] search(String find) {
		SearchRecord[] records = {};
		try {
			String SessionIdValid = usuariosDAO.getSessionIDSF();
			ConnectorConfig config = new ConnectorConfig();
			config.setUsername(Constantes.USERSF);
			config.setPassword(Constantes.PASSWD_SF);
			config.setServiceEndpoint(Constantes.LOGINSF);
			config.setSessionId(SessionIdValid);
			PartnerConnection partnerConnection ;
			partnerConnection = new PartnerConnection(config);
			logger.info("***** Client_ServiceSOA_APISF_WSDAO > search"+ "\n find === " + find);
			SearchResult Respuest = partnerConnection.search(find);
			records = Respuest.getSearchRecords();
		} catch (Exception e) {			
			logger.error("C A T C H        ***** Client_ServiceSOA_APISF_WSDAO > LoginSalesForce()");
			logger.error(e);
			e.printStackTrace();
		}
		return records;
	}
}
