package com.mx.totalplay.dao.ws;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mx.totalplay.utils.Utilerias;
import com.mx.totalplay.vo.LoginResult;
import com.mx.totalplay.vo.ServiceResponse;

@Component
public class ClientRestWSDAO {

	private static final Logger logger = LogManager.getLogger(ClientRestWSDAO.class);
	
	public ServiceResponse consultaGenerica(Object paramsProcess, String URL_SERVICE) {
		logger.info(" UtilidadesDAO   METHOD: consultaGenerica () ");
		logger.info(" URL: " + URL_SERVICE);
		logger.info(" PARAMS: " + Utilerias.jsonUtilitites.toJson(paramsProcess));
		Object responseService = new Object();

		ServiceResponse response = new ServiceResponse();
		response.setSuccess(false);

		try {
			responseService = (Object) Utilerias.consume(paramsProcess, URL_SERVICE, Object.class);

			response.setResult(responseService);
			response.setSuccess(true);

			logger.info("#######RESULT SERVICE ########" + new Gson().toJson(response));

		} catch (Exception e) {
			response.setMensaje(e.getMessage());
			logger.info(" UtilidadesDAO Error General consultaGenerica ");
		}

		return response;
	}
	
	public ServiceResponse consultaGenericaFFM(Object paramsProcess, String URL_SERVICE) {
		logger.info(" UtilidadesDAO   METHOD: consultaGenerica () ");
		logger.info(" URL: " + URL_SERVICE);
		logger.info(" PARAMS: " + Utilerias.jsonUtilitites.toJson(paramsProcess));
		Object responseService = new Object();

		ServiceResponse response = new ServiceResponse();
		response.setSuccess(false);

		try {
			responseService = (Object) Utilerias.consumeFFM(paramsProcess, URL_SERVICE, Object.class);

			response.setResult(responseService);
			response.setSuccess(true);

			logger.info("#######RESULT SERVICE ########" + new Gson().toJson(response));

		} catch (Exception e) {
			response.setMensaje(e.getMessage());
			logger.info(" UtilidadesDAO Error General consultaGenerica ");
		}

		return response;
	}

	
	public LoginResult consultaLogin(Object paramsProcess, String URL_SERVICE) throws UnirestException, Exception {
		logger.info(" UtilidadesDAO   METHOD: consultaGenerica () ");
		logger.info(" URL: " + URL_SERVICE);
		logger.info(" PARAMS: " + Utilerias.jsonUtilitites.toJson(paramsProcess));

		LoginResult loginResult = (LoginResult) Utilerias.consume(paramsProcess, URL_SERVICE, LoginResult.class);

		logger.info("#######RESULT SERVICE ########" + new Gson().toJson(loginResult));

		return loginResult;
	}

}
