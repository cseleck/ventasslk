package com.mx.totalplay.dao;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class LogDAO extends SqlSessionDaoSupport {

	private static final Logger logger = LogManager.getLogger(LogDAO.class);

	@Resource(name = "sqlSessionFactory")
	SqlSessionFactory factory;

	@PostConstruct
	public void init() {
		setSqlSessionFactory(factory);
	}
//
//	public JsonResponse getLog(String id, String start, String end, String draw) {
//		logger.info("***DAO: getLog(" + id + "," + start + ", " + end + ", " + draw + ")");
//		JsonResponse result = new JsonResponse();
//		SqlSession session = null;
//		String[][] dataArray = null;
//		try {
//
//			session = factory.openSession();
//			Map<String, String> mapa = new HashMap<String, String>();
//			mapa.put("start", start);
//			mapa.put("end", end);
//			mapa.put("id", id);
//			int records = session.selectOne("com.mx.totalplay.SDMapper_Selects.countLog");
//			List<LogVO> response = session.selectList("com.mx.totalplay.SDMapper_Selects.getLog", mapa);
//			session.close();
//			dataArray = new String[response.size()][10];
//			logger.info("count: " + records + ", size: " + response.size());
//			if (response.size() > 0) {
//				int cont = 0;
//				for (LogVO log : response) {
//					dataArray[cont][0] = log.getNombre();
//					dataArray[cont][1] = log.getFecha();
//					dataArray[cont][2] = log.getDescripcion();
//					cont++;
//				}
//			}
//			result.setDraw(draw);
//			result.setRecordsTotal(((records == 0) ? response.size() + "" : records + ""));
//			result.setRecordsFiltered(((records == 0) ? response.size() + "" : records + ""));
//			result.setData(dataArray);
//		} catch (Exception e) {
//			logger.error("Error [insertLog]", e);
//			dataArray = new String[0][16];
//			result.setDraw(draw);
//			result.setRecordsTotal("0");
//			result.setRecordsFiltered("0");
//			result.setData(dataArray);
//		} finally {
//			if (session != null)
//				session.close();
//		}
//		return result;
//	}
//
//	public ServiceResponse insertLog(String log, String iddespacho, String idAccion, String ip) {
//		logger.info("***DAO: insertLog()");
//		ServiceResponse result = new ServiceResponse();
//		SqlSession session = null;
//		try {
//
//			session = factory.openSession();
//			String[] logs = log.split(":,");
//			for (String logseparado : logs) {
//				Map<String, String> mapa = new HashMap<String, String>();
//				mapa.put("log", logseparado);
//				mapa.put("id", iddespacho);
//				mapa.put("idAccion", idAccion);
//				mapa.put("dirIp", ip);
//				int report = session.insert("com.mx.totalplay.SDMapper_Inserts.insertLog", mapa);
//				logger.info("result: " + report);
//			}
//
//			session.commit();
//			session.close();
//			result.setSuccess(true);
//
//		} catch (Exception e) {
//			logger.error("Error [insertLog]", e);
//			result.setSuccess(false);
//			result.setMensaje("Error al insertar el log");
//		} finally {
//			if (session != null)
//				session.close();
//		}
//		return result;
//	}

}
