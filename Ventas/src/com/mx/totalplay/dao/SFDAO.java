package com.mx.totalplay.dao;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

import com.mx.totalplay.vo.SFSessionVO;

@Repository
public class SFDAO extends SqlSessionDaoSupport {

	private static final Logger logger = LogManager.getLogger(SFDAO.class);

	@Resource(name = "sqlSessionFactory")
	SqlSessionFactory factory;

	@PostConstruct
	public void init() {
		setSqlSessionFactory(factory);
	}

	public SFSessionVO vigenciaSessionID() {
		logger.info("===SessionIdValid() ===");
		SqlSession session = null;
		SFSessionVO sfSessionVO = null;
		try {
			session = factory.openSession();
			sfSessionVO = session.selectOne("com.mx.totalplay.SDMapper_Selects.getObjIdSessionSF");
		}catch (Exception e) {
			logger.error(e.getMessage());
		}finally{
			if(session!=null)
				session.close();
		}
		return sfSessionVO;
	}
	
	public Boolean actualizaSessionID(String sessionId) {
		logger.info("=== actualizaSessionID() ===");
		SqlSession session = null;
		Boolean actualiza = false;
		try {
				session = factory.openSession();
				System.out.println(  " new SessionId  ::::::::::  "+ sessionId);
				Integer upIdSession = session.update("com.mx.totalplay.SDMapper_Updates.actualizaSessionID",sessionId);
				session.commit();
				if(upIdSession >= 1 ) {
					actualiza = true;
				}
		}catch (Exception e) {
			actualiza = false;
			logger.error(e.getMessage());
		}finally{
			if(session!=null)
				session.close();
		}
		return actualiza;
	}
}
