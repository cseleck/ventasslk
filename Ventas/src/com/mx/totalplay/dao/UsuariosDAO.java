package com.mx.totalplay.dao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.company.Main;
import com.mx.totalplay.dao.ws.ClientSFSOAWSDAO;
import com.mx.totalplay.utils.Constantes;
import com.mx.totalplay.vo.SFSessionVO;
import com.sforce.soap.partner.LoginResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectorConfig;

@Repository
public class UsuariosDAO extends SqlSessionDaoSupport {

	private static final Logger logger = LogManager.getLogger(UsuariosDAO.class);
	
	@Autowired 
	private SFDAO sfdao;
	
	@Autowired
	private ClientSFSOAWSDAO clientSFSOAWSDAO;

	@Resource(name = "sqlSessionFactory")
	SqlSessionFactory factory;

	@PostConstruct
	public void init() {
		setSqlSessionFactory(factory);
	}

	public String validaUsuario(String usuario, String password) {
		logger.info("=== validaUsuario ( usuario = " + usuario + ")");
		SqlSession session = null;
		String idUser;
		try {
			
			System.out.println(Main.encrypt(usuario));
			System.out.println(Main.encrypt(password));
			Map<String,Object> param = new HashMap<String, Object>();
			param.put("usuario", Main.encrypt(usuario));
			param.put("password", Main.encrypt(password));
			session = factory.openSession();
			idUser = session.selectOne("com.mx.totalplay.SDMapper_Selects.validaUsuario", param);
			if(null == idUser || idUser.equals("") || idUser.isEmpty()) {
				idUser = "0";
			}
		} catch (Exception e) {
			idUser = "0";
			logger.error(e.getMessage());
			e.printStackTrace();
			if (e.getCause() instanceof SQLException) {
				SQLException sqlE = (SQLException) e.getCause();
				logger.error("### SQL ERROR ###: [" + sqlE.getErrorCode() + "] " + sqlE.getMessage());
			}
		} finally {
			if (session != null)
				session.close();
		}
		return idUser;
	}
	
	
	
	
	
	
	public int addUser(String vsfu_nombre,String vsfu_user, String cree, int vsfp_id,String vsfu_no_empleado) {
		logger.info("=== addUser ");
		Map<String,Object> hm = new HashMap<String, Object>();
		hm.put("vsfu_nombre", vsfu_nombre);
		hm.put("vsfu_user", vsfu_user);
		hm.put("vsfu_password", cree);
		hm.put("vsfp_id", vsfp_id);
		hm.put("vsfu_no_empleado", vsfu_no_empleado);
		SqlSession session = null;
		int result=0;
		try {
			session = factory.openSession();
			 result=session.insert("com.mx.totalplay.SDMapper_Selects.insert_vsf_usuarios", hm);
		
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			if (e.getCause() instanceof SQLException) {
				SQLException sqlE = (SQLException) e.getCause();
				logger.error("### SQL ERROR ###: [" + sqlE.getErrorCode() + "] " + sqlE.getMessage());
			}
		} finally {
			if (session != null)
				session.close();
		}
		return result;
	}
	
	
	public Map<String,Object> getInfoEmpleado(String idUser) {
		logger.info("=== getInfoEmpleado ( idUser = " + idUser + ")");
		Map<String,Object> result = new HashMap<String, Object>();
		SqlSession session = null;
		try {
			session = factory.openSession();
			result = session.selectOne("com.mx.totalplay.SDMapper_Selects.getInfoEmpleado", idUser);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			if (e.getCause() instanceof SQLException) {
				SQLException sqlE = (SQLException) e.getCause();
				logger.error("### SQL ERROR ###: [" + sqlE.getErrorCode() + "] " + sqlE.getMessage());
			}
		} finally {
			if (session != null)
				session.close();
		}
		return result;
	}
	
	public Map<String,String> getUserInfoSF(String idUserSF){
		logger.info("Service getUserInfoSF("+idUserSF+")");
		Map<String,String> userInfo = new HashMap<String,String>();
		String queryN1 = 
						  "SELECT Id, EmployeeNumber, Name "
						+ "FROM User "
						+ "Where Id = '" + idUserSF + "'";
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("User")) {
					userInfo.put("id",(String) qresultN1[0].getField("Id"));
					userInfo.put("employeeNumber", (String) qresultN1[0].getField("EmployeeNumber"));
					userInfo.put("name", (String) qresultN1[0].getField("Name"));
				}
			}
		}
		return userInfo;
	}
	
	public String getSessionIDSF() {
		logger.info("getSessionIDSF");
	    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
	    Integer diferencia = 20;
	    SFSessionVO sessionIdObj = null;
		    sessionIdObj = sfdao.vigenciaSessionID();
		    diferencia = minutesDiff(GetItemDate(sessionIdObj.getFecha()), GetItemDate(format.format(new Date())) );
		    logger.info("diferencia::" + diferencia);
	    if(diferencia >= 20 ) {
	    	String sessionId = "";
	    	try {
		    	ConnectorConfig config = new ConnectorConfig();
				config.setUsername(Constantes.USERSF);
				config.setPassword(Constantes.PASSWD_SF);
				config.setAuthEndpoint(Constantes.LOGINSF);
				PartnerConnection partnerConnection ;
				partnerConnection = new PartnerConnection(config);
				LoginResult login = partnerConnection.login(Constantes.USERSF, Constantes.PASSWD_SF);
				sessionId = login.getSessionId();
				logger.info("sessionId::" + sessionId);
				sfdao.actualizaSessionID(sessionId);
	    	}catch (Exception e) {
	    		logger.error(e.getMessage());
			}
			return sessionId;
	    }else {
	    	return sessionIdObj.getSessionId();
	    }
	}
	
	public Date GetItemDate(final String date){
	    final Calendar cal = Calendar.getInstance(TimeZone.getDefault());
	    final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
	    format.setCalendar(cal);
	    try {
	        return format.parse(date);
	    } catch (Exception e) {
	    	logger.error(e.getMessage());
	    	return null;
	    }
	}
	
	public Integer minutesDiff(Date earlierDate, Date laterDate){
	    if( earlierDate == null || laterDate == null ) return 0;
	    return (int)((laterDate.getTime()/60000) - (earlierDate.getTime()/60000));
	}

}
