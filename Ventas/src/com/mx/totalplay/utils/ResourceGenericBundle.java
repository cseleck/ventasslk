package com.mx.totalplay.utils;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ResourceGenericBundle {

	public static String getProperties(String sproperties) {

		ResourceBundle bundle = null;
		String cadena = "";
		
		try {
			bundle = ResourceBundle.getBundle(Constantes.WS_FFM_PROP, Locale.getDefault());
		} catch (MissingResourceException e) {
			bundle = null;
		}
		
		if (bundle != null) {
			try {
				cadena = bundle.getString(sproperties);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return cadena;
		}

		return null;
	}

}
