package com.mx.totalplay.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.company.Main;
import com.mx.totalplay.vo.ServiceResponse;

@SuppressWarnings({ "deprecation"})
public class upload {

	private static final Logger logger = LogManager.getLogger(upload.class);

	public upload() {
		super();
	}

	ServiceResponse result = new ServiceResponse();

	@SuppressWarnings({ "resource" })
	public ServiceResponse uploadFile(File inFile, String name, String string) {
		FileInputStream fis = null;
		logger.info("inFile: " + inFile.isFile() + ", path: " + name + ", name: " + string);
		try {
			fis = new FileInputStream(inFile);

			DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
			HttpPost httppost = new HttpPost("https://mss.totalplay.com.mx/FFMTpe/WriteFile");
			httppost.setHeader("Authorization", "Basic ZmZtYXBwOjRnZW5kNG1pM250bw==");

			MultipartEntity entity = new MultipartEntity();
			logger.info("name: , inFile: " + string);
			entity.addPart("File", new InputStreamBody(fis, string));
			entity.addPart("FilePath", new StringBody(Main.encrypt(name)));
			logger.info(entity.toString());
			httppost.setEntity(entity);
			HttpResponse response = httpclient.execute(httppost);
			int statusCode = response.getStatusLine().getStatusCode();
			logger.info(statusCode);
			logger.info("CODE : " + statusCode);

			if (statusCode == 200) {
				result.setSuccess(true);
			} else {
				result.setMensaje("No fue posible guardar la imagen");
				result.setSuccess(false);
			}

		} catch (ClientProtocolException e) {
			logger.error("No se puede establecer la conexi�n", e);
		} catch (IOException e) {
			logger.error("Imposible leer el archivo", e);
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				logger.error("Error: ", e);
			}
		}
		return result;
	}

	public static String encoderImg(String imagePath) {
		String base64Image = "";
		File file = new File(imagePath);
		try (FileInputStream imageInFile = new FileInputStream(file)) {
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read(imageData);
			base64Image = Base64.getEncoder().encodeToString(imageData);
		} catch (FileNotFoundException e) {
		} catch (IOException ioe) {
		}

		return base64Image;
	}

}
