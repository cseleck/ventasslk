package com.mx.totalplay.utils;

public final class Constantes {
	
//	public static final String WS_FFM_PROP = "com.mx.totalplay.utils.ws_QA";
	public static final String WS_FFM_PROP = "com.mx.totalplay.utils.ws_Prod";

	public static final String PASSWORD = ResourceGenericBundle.getProperties("ws.login.pass");
	public static final String USUARIO = ResourceGenericBundle.getProperties("ws.login.usuario");
	public static final String IP = ResourceGenericBundle.getProperties("ws.login.ip");
	
	public static final String USERSF = ResourceGenericBundle.getProperties("ws.sf.user");
	public static final String PASSWD_SF = ResourceGenericBundle.getProperties("ws.sf.passwd");
	public static final String ID_USER_SF = ResourceGenericBundle.getProperties("ws.sf.iduserSF");
	
	public static final String HEADER_ACCEPT = "accept";
	public static final String KEY_AUTORIZATHION = "Authorization";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String HEADER_AUTORIZATHION = "Basic ZmZtYXBwOjRnZW5kNG1pM250bw==";
	
	public static final String LOGINSF = ResourceGenericBundle.getProperties("ws.sf.login");
	
	public static final String WS_URL_HISTORIAL_SITIO = ResourceGenericBundle.getProperties("ws.url.historialSitio");


	
}