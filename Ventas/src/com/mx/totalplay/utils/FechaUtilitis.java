package com.mx.totalplay.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FechaUtilitis {
	
	public static Date sumarRestarDiasFecha(Date fecha, int dias) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.DAY_OF_YEAR, dias); // numero de d�as a a�adir, o restar en caso de d�as<0
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos d�as a�adidos
	}

	public String formatearSegundos(double doubleSegundos) {
		long millis = (long) doubleSegundos * 1000;
		String myTime = String.format("%02d:%02d:%02d",
				// Hours
				TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),
				// Minutes
				TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
				// Seconds
				TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		return myTime;
	}

}
