package com.mx.totalplay.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class Utility {
	private static final Logger logger = LogManager.getLogger();
	
	public String fileToB64(MultipartFile file) {
		String strB64 = null;
		try {
			InputStream fileContent = file.getInputStream();
			byte[] buff = new byte[8000];
			int bytesRead = 0;
			ByteArrayOutputStream bao = new ByteArrayOutputStream();
			while ((bytesRead = fileContent.read(buff)) != -1) {
				bao.write(buff, 0, bytesRead);
			}
			byte[] data = bao.toByteArray();
			byte[] encoded = Base64.encodeBase64(data);
			strB64 = new String(encoded);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Utility > fileToB64" + e.getMessage());
		}
		return strB64;
	}
}
