package com.mx.totalplay.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class Utilerias {
	
	private static final Logger logger = LogManager.getLogger(Utilerias.class);
	public static  Gson jsonUtilitites=new Gson();
	/**
	 * Metodo que consume un servicio rest y mapea el resultado a una objeto de una clase deseada
	 * @param process_parameters objeto con los parametros de envio al servicio
	 * @param url_peticion ulr_peticion URL de la peticion
	 * @param conversion .class de conversion
	 * 
	 * @return
	 */
	public static Object consume(Object process_parameters,String url_peticion, Class<?> classConversion ) throws UnirestException,Exception{	
		logger.info("#### URL CONSUME ###"+url_peticion);
		String responseJSONString ="";
		try {
				Map<String, String> headers = new HashMap<String, String>();
			    headers.put( Constantes.HEADER_ACCEPT,		 Constantes.APPLICATION_JSON   );
			    headers.put( Constantes.HEADER_CONTENT_TYPE,  Constantes.APPLICATION_JSON  );	
			    headers.put( Constantes.KEY_AUTORIZATHION,  Constantes.HEADER_AUTORIZATHION  );	
			    
				HttpResponse<JsonNode> response  = Unirest.post( url_peticion ).headers(headers)
						.body( new Gson().toJson(process_parameters) ).asJson();
				
				int status=response.getStatus();
		        
		        if(status >= 500){
		        	throw new Exception("Error interno del servidor.");
		        }
		        if(status == 404){
		        	throw new Exception("Recurso solicitado no encontrado en servidor.");
		        }         	
		        if(status >= 400 && status<500 ){
		        	throw new Exception("Peticion Erronea");
		        }
		        if(status >= 300 && status<400 ){
		        	throw new Exception("Url no especificada lo suficiente");
		        }
				
				responseJSONString = response.getBody().toString();	
				logger.info(responseJSONString);
			} catch (UnirestException e) {
				logger.error("Error UnirestException in service "+url_peticion+"\n"+e);
				throw new UnirestException(e);
			}catch (Exception e) {
				e.printStackTrace();
				logger.error("Exception e in service " + url_peticion);
				throw new Exception("Error General");
			}
		return jsonUtilitites.fromJson(responseJSONString,classConversion);
	}
		
	public static Object consumeFFM(Object process_parameters,String url_peticion, Class<?> classConversion ) throws UnirestException,Exception{	
		logger.info("#### URL CONSUME ###"+url_peticion);
		String responseJSONString ="";
		try {
				Map<String, String> headers = new HashMap<String, String>();
			    headers.put( Constantes.HEADER_ACCEPT,		 Constantes.APPLICATION_JSON   );
			    headers.put( Constantes.HEADER_CONTENT_TYPE,  Constantes.APPLICATION_JSON  );			 
			    
				HttpResponse<JsonNode> response  = Unirest.post( url_peticion ).headers(headers)
						.body( new Gson().toJson(process_parameters) ).asJson();
				
				int status=response.getStatus();
		        
		        if(status >= 500){
		        	throw new Exception("Error interno del servidor.");
		        }
		        if(status == 404){
		        	throw new Exception("Recurso solicitado no encontrado en servidor.");
		        }         	
		        if(status >= 400 && status<500 ){
		        	throw new Exception("Peticion Erronea");
		        }
		        if(status >= 300 && status<400 ){
		        	throw new Exception("Url no especificada lo suficiente");
		        }
				
				responseJSONString = response.getBody().toString();	
				logger.info(responseJSONString);
			} catch (UnirestException e) {
				logger.error("Error UnirestException in service "+url_peticion+"\n"+e);
				throw new UnirestException(e);
			}catch (Exception e) {
				e.printStackTrace();
				logger.error("Exception e in service " + url_peticion);
				throw new Exception("Error General");
				// TODO Auto-generated catch block
			}
		return jsonUtilitites.fromJson(responseJSONString,classConversion);
	}		
	
	public static boolean isParseoNumero(String texto_numero){
	    boolean parsable = true;
	    try{
	        Long.parseLong(texto_numero);
	    }catch(NumberFormatException e){
	        parsable = false;
	        logger.info("Numero incorrecto excel"+texto_numero+"##");
	    }
	    return parsable;
	}
	
}
