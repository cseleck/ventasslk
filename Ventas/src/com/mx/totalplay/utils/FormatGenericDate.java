package com.mx.totalplay.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatGenericDate {
	public static String formatFechaBpelComplete(String date) {
		SimpleDateFormat formateadorTradicional = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formateadorBpel = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String nuevaFecha = "";
		Date dateTradicional = null;
		try {
			dateTradicional = formateadorTradicional.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		nuevaFecha = formateadorBpel.format(dateTradicional);
		return nuevaFecha;
	}

	public static void main(String args[]) {
		System.out.println(FormatGenericDate.formatFechaBpelComplete("12/11/2016"));
	}
}
