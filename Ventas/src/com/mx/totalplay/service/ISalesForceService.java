package com.mx.totalplay.service;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.mx.totalplay.vo.ServiceResponse;

public interface ISalesForceService {
	
	public ServiceResponse getChatterCot(String idCot);

	public ServiceResponse getFileContentVersion(String idFile);

	public ServiceResponse insertChartterCot(MultipartFile file, String idParent, String textCharter, String idEmpleado, String nivel);

	public ServiceResponse getCotsVendedor(String idVendedor);

	public ServiceResponse getDetalleCot(String idCot);

	public ServiceResponse getSitiosCot(String idCot);

	public ServiceResponse getHistorialSitio(String idOS);

	public Map<String, Object> getEstatusSitio(String idCot);

	public ServiceResponse getPlanesSitiosCot(String idSitio);

}
