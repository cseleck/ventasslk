package com.mx.totalplay.service;

import com.mx.totalplay.vo.ServiceResponse;

public interface IMesaControlService {
	
	public ServiceResponse getUltimasCotsMC();
	
	public ServiceResponse getListCotsMC();

	public ServiceResponse getListDocsCot(String idCot);

	public ServiceResponse updateAprobarMesa(String idCot, String idOportunidad, String folioCredito, Boolean caratulaContrato,
			Boolean clausulado, Boolean rFC, Boolean buroCredito, Boolean actaConstitutiva, Boolean estadosCuenta,
			Boolean comprobanteDomicilio, Boolean identificacionOficial, Boolean consentimientoVista, Boolean tryAndBuy,
			String respuesta, String motivo, String comentarios);

}
