package com.mx.totalplay.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.totalplay.dao.SFDAO;
import com.mx.totalplay.service.ISFService;
import com.mx.totalplay.utils.Constantes;
import com.mx.totalplay.vo.SFSessionVO;
import com.sforce.soap.partner.LoginResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectorConfig;

@Service
public class SFServiceImpl implements ISFService{
	
	private static final Logger logger = LogManager.getLogger(SFServiceImpl.class);

	@Autowired 
	private SFDAO sfdao;
	
	@Override
	public String getSessionIDSF() {
		logger.info("getSessionIDSF");
	    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
	    Integer diferencia = 20;
	    SFSessionVO sessionIdObj = null;
		    sessionIdObj = sfdao.vigenciaSessionID();
		    diferencia = minutesDiff(GetItemDate(sessionIdObj.getFecha()), GetItemDate(format.format(new Date())) );
		    logger.info("diferencia::" + diferencia);
	    if(diferencia >= 20 ) {
	    	String sessionId = "";
	    	try {
		    	ConnectorConfig config = new ConnectorConfig();
				config.setUsername(Constantes.USERSF);
				config.setPassword(Constantes.PASSWD_SF);
				config.setAuthEndpoint(Constantes.LOGINSF);
				PartnerConnection partnerConnection ;
				partnerConnection = new PartnerConnection(config);
				LoginResult login = partnerConnection.login(Constantes.USERSF, Constantes.PASSWD_SF);
				sessionId = login.getSessionId();
				logger.info("sessionId::" + sessionId);
				sfdao.actualizaSessionID(sessionId);
	    	}catch (Exception e) {
	    		logger.error(e.getMessage());
			}
			return sessionId;
	    }else {
	    	return sessionIdObj.getSessionId();
	    }
	}
	
	public Date GetItemDate(final String date){
	    final Calendar cal = Calendar.getInstance(TimeZone.getDefault());
	    final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
	    format.setCalendar(cal);
	    try {
	        return format.parse(date);
	    } catch (Exception e) {
	    	logger.error(e.getMessage());
	    	return null;
	    }
	}
	
	public Integer minutesDiff(Date earlierDate, Date laterDate){
	    if( earlierDate == null || laterDate == null ) return 0;
	    return (int)((laterDate.getTime()/60000) - (earlierDate.getTime()/60000));
	}


}
