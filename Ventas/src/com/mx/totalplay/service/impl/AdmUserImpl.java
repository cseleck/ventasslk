package com.mx.totalplay.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mx.totalplay.service.IAdmUser;
import com.mx.totalplay.vo.ServiceResponse;

public class AdmUserImpl implements IAdmUser{
	
	private static final Logger logger = LogManager.getLogger(MesaControlImpl.class);

	@Override
	public ServiceResponse addUser() {
		logger.info("Service addUser()");
		ServiceResponse response = new ServiceResponse();
		
		
		response.setMensaje("addUser");
		response.setResult(null);
		response.setSuccess(true);
		return response;
	}

}
