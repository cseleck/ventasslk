package com.mx.totalplay.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mx.totalplay.dao.ws.ClientRestWSDAO;
import com.mx.totalplay.dao.ws.ClientSFSOAWSDAO;
import com.mx.totalplay.service.ISalesForceService;
import com.mx.totalplay.utils.Constantes;
import com.mx.totalplay.utils.Utility;
import com.mx.totalplay.vo.ServiceResponse;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;


@Service
public class SalesForceServiceImpl implements ISalesForceService{
	
	private static final Logger logger = LogManager.getLogger(SalesForceServiceImpl.class);

	@Autowired
	private ClientSFSOAWSDAO clientSFSOAWSDAO;
	
	@Autowired
	private ClientRestWSDAO clientRestWSDAO;
	
	@Autowired
	private Utility utility;
	
	
	@Override
	public ServiceResponse getChatterCot(String idCot) {
		logger.info("Service getChatterCot("+idCot+")");
		ServiceResponse response = new ServiceResponse();
		List<Object> listComentarios = new ArrayList<Object>();
		
		String queryN1 = 
						  "SELECT RelatedRecordId, Body, CreatedBy.Name, FORMAT(CreatedDate) CreatedDate, Id "
						+ "FROM Cotizacion__Feed "
						+ "WHERE ParentId = '" + idCot + "' "
						+ "ORDER BY CreatedDate DESC";
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				String queryN2 = 
								  "SELECT CommentBody, CommentType, CreatedBy.Name, FORMAT(CreatedDate) CreatedDate, FeedItemId, Id, RelatedRecordId "
								+ "FROM FeedComment "
								+ "WHERE FeedItemId IN ( "
														+ "SELECT Id "
														+ "FROM Cotizacion__Feed "
														+ "WHERE ParentId = '" + idCot + "' "
													+ " ) "
								+ "ORDER BY CreatedDate ASC";
				QueryResult comentariosN2 = clientSFSOAWSDAO.query(queryN2);
				if (comentariosN2.getDone()) {
					SObject[] qresultN2= comentariosN2.getRecords();
					if (qresultN2.length > 0) {
						for (int i = 0; i < qresultN1.length; i++) {
							if (qresultN1[i].getType().equals("Cotizacion__Feed")) {
								Map<String,Object> comentarioCot = new HashMap<String, Object>();
								
								String body = (String)qresultN1[i].getField("Body");
								String[] arrBody = null;
								
								if ( !(body == null || body.isEmpty()) ) {
									arrBody = body.split(" @@ ", 2);
								}
								
								String to="";
								
								if(null != arrBody && arrBody.length == 2) {
									String [] arrTo = arrBody[0].split("_");
									to = arrTo[0] + " " +arrTo[1];
									body = arrBody[1];
								}else {
									to = (String) qresultN1[i].getChild("CreatedBy").getField("Name");
								}
								
								comentarioCot.put("id", qresultN1[i].getField("Id"));
								comentarioCot.put("name", to);
								comentarioCot.put("body", body);
								comentarioCot.put("date", qresultN1[i].getField("CreatedDate"));
								comentarioCot.put("idDoc", qresultN1[i].getField("RelatedRecordId"));
								String id = (String) qresultN1[i].getField("Id");
								List<Object> listSubComentario = new ArrayList<Object>();
								for (int j = 0; j < qresultN2.length; j++) {
									if (qresultN2[j].getType().equals("FeedComment")) {
										String feedItemId =  (String)qresultN2[j].getField("FeedItemId");
										if(id.equals(feedItemId)) {
											Map<String,Object> subComentario = new HashMap<String, Object>();
											
											String bodyS = (String) qresultN2[j].getField("CommentBody");
											String[] arrBodyS = null;
											if ( !(bodyS == null || bodyS.isEmpty()) ) {
												arrBodyS = bodyS.split(" @@ ", 2);
											}
											
											String toS="";
											
											if(null != arrBodyS && arrBodyS.length == 2) {
												String [] arrToS = arrBodyS[0].split("_");
												toS = arrToS[0] + " " +arrToS[1];
												bodyS = arrBodyS[1];
											}else {
												toS = (String) qresultN2[j].getChild("CreatedBy").getField("Name");
											}
											
											subComentario.put("name", toS);
											subComentario.put("date", qresultN2[j].getField("CreatedDate"));
											subComentario.put("body", bodyS);
											subComentario.put("idDoc", qresultN2[j].getField("RelatedRecordId"));
											listSubComentario.add(subComentario);
										}
									}
								}
								comentarioCot.put("subComentario", listSubComentario);
								listComentarios.add(comentarioCot);
							}
						}
					}else {
						List<Object> listSubComentario = new ArrayList<Object>();
						for (int i = 0; i < qresultN1.length; i++) {
							if (qresultN1[i].getType().equals("Cotizacion__Feed")) {
								Map<String,Object> comentarioCot = new HashMap<String, Object>();
								
								String body = (String)qresultN1[i].getField("Body");
								String[] arrBody = null;
								
								if ( !(body == null || body.isEmpty()) ) {
									arrBody = body.split(" @@ ", 2);
								}
								
								String to="";
								
								if(null != arrBody && arrBody.length == 2) {
									String [] arrTo = arrBody[0].split("_");
									to = arrTo[0] + " " +arrTo[1];
									body = arrBody[1];
								}else {
									to = (String) qresultN1[i].getChild("CreatedBy").getField("Name");
								}
								
								comentarioCot.put("id", qresultN1[i].getField("Id"));
								comentarioCot.put("name", to);
								comentarioCot.put("body", body);
								comentarioCot.put("date", qresultN1[i].getField("CreatedDate"));
								comentarioCot.put("idDoc", qresultN1[i].getField("RelatedRecordId"));
								comentarioCot.put("subComentario", listSubComentario);
								listComentarios.add(comentarioCot);
							}
						}
					}
				}else {
					List<Object> listSubComentario = new ArrayList<Object>();
					for (int i = 0; i < qresultN1.length; i++) {
						if (qresultN1[i].getType().equals("Cotizacion__Feed")) {
							Map<String,Object> comentarioCot = new HashMap<String, Object>();
							
							String body = (String)qresultN1[i].getField("Body");
							String[] arrBody = null;
							
							if ( !(body == null || body.isEmpty()) ) {
								arrBody = body.split(" @@ ", 2);
							}
							
							String to="";
							
							if(null != arrBody && arrBody.length == 2) {
								String [] arrTo = arrBody[0].split("_");
								to = arrTo[0] + " " +arrTo[1];
								body = arrBody[1];
							}else {
								to = (String) qresultN1[i].getChild("CreatedBy").getField("Name");
							}
							
							comentarioCot.put("id", qresultN1[i].getField("Id"));
							comentarioCot.put("name", to);
							comentarioCot.put("body", body);
							comentarioCot.put("date", qresultN1[i].getField("CreatedDate"));
							comentarioCot.put("idDoc", qresultN1[i].getField("RelatedRecordId"));
							comentarioCot.put("subComentario", listSubComentario);
							listComentarios.add(comentarioCot);
						}
					}
				}
			}
		}
		response.setMensaje("Lista  Comentarios");
		response.setResult(listComentarios);
		response.setSuccess(true);
		return response;
	}
	
	@Override
	public ServiceResponse getFileContentVersion(String idFile) {
		logger.info("Service getFileChatterCot("+idFile+")");
		ServiceResponse response = new ServiceResponse();
		Map<String,Object> fileChatterCot = new HashMap<String, Object>();
		String queryN1 = 
						  "SELECT FileExtension, FileType, Title, VersionData "
					    + "FROM ContentVersion "
					    + "Where Id = '" + idFile +"'";
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("ContentVersion")) {
					fileChatterCot.put("fileExtension", qresultN1[0].getField("FileExtension"));
					fileChatterCot.put("fileType", qresultN1[0].getField("FileType"));
					fileChatterCot.put("title", qresultN1[0].getField("Title"));
					fileChatterCot.put("dataFile", qresultN1[0].getField("VersionData"));
				}
			}
		}
		response.setMensaje("File  Comentario");
		response.setResult(fileChatterCot);
		response.setSuccess(true);
		return response;
	}
	
	@Override
	public ServiceResponse insertChartterCot(MultipartFile file, String idParent, String textCharter, String idEmpleado, String nivel) {
		logger.info(file.getContentType());
		logger.info(file.getName());
		logger.info(file.getOriginalFilename());
		
		ServiceResponse response = new ServiceResponse();
		String RelatedRecordId = " ";
		if(file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")) {
			String fileBase64 = utility.fileToB64(file);
			if (fileBase64 != null) {
				SObject ContentVersion = new SObject();
				ContentVersion.setType("ContentVersion");
				ContentVersion.setField("PathOnClient", file.getOriginalFilename());
				ContentVersion.setField("VersionData", Base64.decodeBase64(fileBase64));
				String title = (file.getOriginalFilename().substring(0, file.getOriginalFilename().lastIndexOf('.')));
				ContentVersion.setField("Title", title);
				
				SaveResult[] resultData = clientSFSOAWSDAO.create(new SObject[] { ContentVersion });
				for (int i = 0; i < resultData.length; i++) {
					if (resultData[i].isSuccess()) {
						logger.info("Successfully created ID: " + resultData[i].getId());
						RelatedRecordId = (String)resultData[i].getId();
					} else {
						logger.info("Error: could not create sobject " + "for array element " + i + ".");
						logger.info("   The error reported was: " + resultData[i].getErrors()[i].getMessage() + "\n");
						response.setSuccess(false);
						response.setMensaje(resultData[i].getErrors()[i].getMessage());
						return response;
					}
				}
			} else {
				response.setSuccess(false);
				response.setMensaje("Error en la conversion del archivo");
				return response;
			}
		}
		if(nivel.equals("1")) {
			SObject FeedItem = new SObject();
			FeedItem.setType("FeedItem");
			FeedItem.setField("Body", textCharter);
			FeedItem.setField("CreatedById", idEmpleado);
			FeedItem.setField("ParentId", idParent);
			FeedItem.setField("IsRichText", true);
			FeedItem.setField("RelatedRecordId", RelatedRecordId);
			SaveResult[] resultData = clientSFSOAWSDAO.create(new SObject[] { FeedItem });
			for (int i = 0; i < resultData.length; i++) {
				if (resultData[i].isSuccess()) {
					response.setSuccess(true);
					response.setMensaje("Comentario Guardado.");
				} else {
					logger.info("Error: could not create sobject " + "for array element " + i + ".");
					logger.info("   The error reported was: " + resultData[i].getErrors()[i].getMessage() + "\n");
					response.setSuccess(false);
					response.setMensaje(resultData[i].getErrors()[i].getMessage());
					return response;
				}
			}
		}else if(nivel.equals("2")){
			SObject FeedComment = new SObject();
			FeedComment.setType("FeedComment");
			FeedComment.setField("CommentBody", textCharter);
			FeedComment.setField("CreatedById", idEmpleado);
			FeedComment.setField("FeedItemId", idParent);
			FeedComment.setField("RelatedRecordId", RelatedRecordId);
			
			SaveResult[] resultData = clientSFSOAWSDAO.create(new SObject[] { FeedComment });
			for (int i = 0; i < resultData.length; i++) {
				if (resultData[i].isSuccess()) {
					response.setSuccess(true);
					response.setMensaje("Comentario Guardado.");
				} else {
					logger.info("Error: could not create sobject " + "for array element " + i + ".");
					logger.info("   The error reported was: " + resultData[i].getErrors()[i].getMessage() + "\n");
					response.setSuccess(false);
					response.setMensaje(resultData[i].getErrors()[i].getMessage());
					return response;
				}
			}
		}
		return response;
	}
	
	
	@Override
	public ServiceResponse getCotsVendedor(String idVendedor) {
		logger.info("Service getCotsVendedor("+idVendedor+")");
		ServiceResponse response = new ServiceResponse();
		List<Map<String,Object>> listCotsVendedor = new ArrayList<Map<String,Object>>();
		String queryN1 = 
						  "SELECT "
								  + "Id, Name, Nombre__c, Oportunidad__r.Name,Oportunidad__r.Account.RazonSocial__c,FORMAT(CreatedDate) CreatedDate, Estatus__c  "
						+ "FROM Cotizacion__c "
						+ "Where OwnerId = '" + idVendedor + "' AND RecordType.Name = 'Enlace' AND CotizacionPrincipal__c = true "
						+ "Order By CreatedDate DESC ";
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("Cotizacion__c")) {
					for (int i = 0; i < qresultN1.length; i++) {
						Map<String,Object> cotVendedor = new HashMap<String, Object>();
						cotVendedor.put("id", 			(String)qresultN1[i].getField("Id"));
						cotVendedor.put("name",  		(String)qresultN1[i].getField("Name"));
						cotVendedor.put("nombre",  		(String)qresultN1[i].getField("Nombre__c"));
						cotVendedor.put("estatus", (String)qresultN1[i].getField("Estatus__c"));
						if(null != qresultN1[0].getChild("Oportunidad__r")) {
							cotVendedor.put("oportunidad",(String)qresultN1[i].getChild("Oportunidad__r").getField("Name"));
							if(null != qresultN1[i].getChild("Oportunidad__r").getChild("Account")) {
								cotVendedor.put("razonSocial", (String)qresultN1[i].getChild("Oportunidad__r").getChild("Account").getField("RazonSocial__c"));
							}
						}
						cotVendedor.put("fecha",		(String)qresultN1[i].getField("CreatedDate"));
						listCotsVendedor.add(cotVendedor);
					}
				}
			}
		}
		response.setMensaje("Lista  Cots");
		response.setResult(listCotsVendedor);
		response.setSuccess(true);
		return response;
	}
	
	@Override
	public ServiceResponse getDetalleCot(String idCot) {
		logger.info("Service getDetalleCot("+idCot+")");
		ServiceResponse response = new ServiceResponse();
		Map<String,Object> detalleCot = new HashMap<String, Object>();
		String queryN1 = 
						  "SELECT "
								  + "Id, Name, Nombre__c, " 
								  
								  + "Oportunidad__r.Name, Oportunidad__r.NumeroOportunidad__c, Oportunidad__r.Type, Oportunidad__r.Subtipo__c, "
								  + "Oportunidad__r.LeadSource, Oportunidad__r.SegmentoFacturacion__c, Oportunidad__r.StageName, Oportunidad__r.Probability, Oportunidad__r.forma_pago__c, "
								  
						  		  + "Oportunidad__r.Account.RazonSocial__c, Oportunidad__r.Account.Name, Oportunidad__r.Account.TipoPersona__c, "
						  		  + "Oportunidad__r.Account.Phone, Oportunidad__r.Account.Industry, Oportunidad__r.Account.ContactoPrincipal__r.Name " 
						  		  
						  + "FROM Cotizacion__c " 
						  + "WHERE Id = '" + idCot + "' ";
		
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("Cotizacion__c")) {
						
					detalleCot.put("idCot", 		 (String)qresultN1[0].getField("Id"));
					detalleCot.put("folioCot", 		 (String)qresultN1[0].getField("Name"));
					detalleCot.put("nombreCot", 	 (String)qresultN1[0].getField("Nombre__c"));
					if(null != qresultN1[0].getChild("Oportunidad__r")) {
						detalleCot.put("nombreOpor", 	 (String)qresultN1[0].getChild("Oportunidad__r").getField("Name"));
						detalleCot.put("NumeroOpor", 	 (String)qresultN1[0].getChild("Oportunidad__r").getField("NumeroOportunidad__c"));
						detalleCot.put("tipoOpor", 		 (String)qresultN1[0].getChild("Oportunidad__r").getField("Type"));
						detalleCot.put("subTipoOpor", 	 (String)qresultN1[0].getChild("Oportunidad__r").getField("Subtipo__c"));
						detalleCot.put("origenOpor", 	 (String)qresultN1[0].getChild("Oportunidad__r").getField("LeadSource"));
						detalleCot.put("segmentoOpor", 	 (String)qresultN1[0].getChild("Oportunidad__r").getField("SegmentoFacturacion__c"));
						detalleCot.put("etapaOpor", 	 (String)qresultN1[0].getChild("Oportunidad__r").getField("StageName"));
						detalleCot.put("probaOpor", 	 (String)qresultN1[0].getChild("Oportunidad__r").getField("Probability"));
						detalleCot.put("formaPagoOpor", 	 (String)qresultN1[0].getChild("Oportunidad__r").getField("forma_pago__c"));
						if(null != qresultN1[0].getChild("Oportunidad__r").getChild("Account")) {
							detalleCot.put("nombreCta", 	 qresultN1[0].getChild("Oportunidad__r").getChild("Account").getField("Name"));
							detalleCot.put("razonSocCta", 	 qresultN1[0].getChild("Oportunidad__r").getChild("Account").getField("RazonSocial__c"));
							detalleCot.put("tipoPersonaCta", qresultN1[0].getChild("Oportunidad__r").getChild("Account").getField("TipoPersona__c"));
							detalleCot.put("telCta", 		 qresultN1[0].getChild("Oportunidad__r").getChild("Account").getField("Phone"));
							detalleCot.put("sectorCta", 	 qresultN1[0].getChild("Oportunidad__r").getChild("Account").getField("Industry"));
							detalleCot.put("contactoCta", 	 qresultN1[0].getChild("Oportunidad__r").getChild("Account").getChild("ContactoPrincipal__r").getField("Name"));
						}
					}
				}
			}
		}
		response.setMensaje("File  Comentario");
		response.setResult(detalleCot);
		response.setSuccess(true);
		return response;
	}
	
	@Override
	public ServiceResponse getSitiosCot(String idCot) {
		logger.info("Service getSitiosCot( " + idCot + " )");
		ServiceResponse response = new ServiceResponse();
		List<Map<String,Object>> listSitiosCot = new ArrayList<Map<String,Object>>();
		String queryN1 = 
						  "SELECT " + 
						  "Id, Sitio__r.Name, DireccionSitio__c, OrdenServicio__r.IdOtGIM__c, OrdenServicio__r.Name, Cotizacion__c " + 
						  "FROM Cot_Sitio__c " + 
						  "WHERE Cotizacion__c = '" + idCot + "'";
		
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("Cot_Sitio__c")) {
					for (int i = 0; i < qresultN1.length; i++) {
						
						Map<String,Object> sitioCot = new HashMap<String, Object>();
						sitioCot.put("name", (String)  qresultN1[i].getChild("Sitio__r").getField("Name"));
						sitioCot.put("idSitio", (String)  qresultN1[i].getField("Id"));
						sitioCot.put("direccion", (String)  qresultN1[i].getField("DireccionSitio__c"));
						sitioCot.put("idOt", (String)  qresultN1[i].getChild("OrdenServicio__r").getField("IdOtGIM__c"));
						sitioCot.put("idOs", (String)  qresultN1[i].getChild("OrdenServicio__r").getField("Name"));
						sitioCot.put("idCot", (String)  qresultN1[i].getField("Cotizacion__c"));
						
						listSitiosCot.add(sitioCot);
						
					}
				}
			}
		}
		response.setMensaje("Lista  Sitios Cot");
		response.setResult(listSitiosCot);
		response.setSuccess(true);
		return response;
	}
	
	@Override
	public ServiceResponse getHistorialSitio(String idOS) {
		logger.info("Service getHistorialSitio( " + idOS + " )");
		ServiceResponse result = new ServiceResponse();
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> login = new HashMap<String, Object>();
		login.put("User", Constantes.USUARIO);
		login.put("Password", Constantes.PASSWORD);
		login.put("Ip", Constantes.IP);
		params.put("Login", login);
		params.put("OS", idOS);
		result = clientRestWSDAO.consultaGenericaFFM(params,Constantes.WS_URL_HISTORIAL_SITIO);
		return result;
	}
	
	@Override
	public  Map<String, Object> getEstatusSitio(String idCot) {
		logger.info("Service getEstatusMesa()");
		Map<String,Object> estatusMesa = new HashMap<String, Object>();
		String queryN1 = 
						    "SELECT SolicitarApoyoMesa__c, Oportunidad__r.StageName, Oportunidad__r.ValidacionMesa__c, Oportunidad__r.RechazoAnte_MC__c " 
						    + "FROM Cotizacion__c " 
						    + "Where Id = '" + idCot + "'";
		
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("Cotizacion__c")) {
					for (int i = 0; i < qresultN1.length; i++) {
						
						if( ((String)qresultN1[i].getChild("Oportunidad__r").getField("StageName")).equals("Ganada") ) {
							 estatusMesa.put("mesa", true);
							 estatusMesa.put("mesaDetalle", "Ganada");
						 }else if( ((String)qresultN1[i].getChild("Oportunidad__r").getField("StageName")).equals("Perdida") ) {
							 estatusMesa.put("mesa", true);
							 estatusMesa.put("mesaDetalle", "Rechazada");
						 }else if( ((String)qresultN1[i].getChild("Oportunidad__r").getField("StageName")).equals("Credito") && ((String)qresultN1[i].getField("SolicitarApoyoMesa__c")).equals("true")) {
							 estatusMesa.put("mesa", true);
							 estatusMesa.put("mesaDetalle", "Validación");
						 }else {
							 estatusMesa.put("mesa", false);
							 estatusMesa.put("mesaDetalle", "");
						 }
					}
				}
			}
		}
		return estatusMesa;
	}

	@Override
	public ServiceResponse getPlanesSitiosCot(String idSitio) {
		logger.info("Service getPlanesSitiosCot( " + idSitio + " )");
		ServiceResponse response = new ServiceResponse();
		List<Map<String,Object>> listSitiosCot = new ArrayList<Map<String,Object>>();
		String queryN1 = 
						  "SELECT Id, NombrePlan__c, Cotizacion__c " + 
						  "FROM Cot_SitioPlan__c " + 
						  "WHERE Cot_Sitio__c = '" + idSitio + "'";
		
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			String queryN2 = 
					  "SELECT Name,BCot_SitioPlan__c " + 
					  "FROM OrdenServicio__c " + 
					  "WHERE BCot_SitioPlan__r.Cot_Sitio__c ='" + idSitio + "'";
			
			QueryResult comentariosN2 = clientSFSOAWSDAO.query(queryN2);
			if (comentariosN2.getDone()) {
				SObject[] qresultN2 = comentariosN2.getRecords();
				if (qresultN2.length > 0) {
					if (qresultN2[0].getType().equals("OrdenServicio__c") && qresultN1[0].getType().equals("Cot_SitioPlan__c")) {
						for (int i = 0; i < qresultN1.length; i++) {						
							Map<String,Object> planSitio = new HashMap<String, Object>();
							planSitio.put("nommbre", (String)  qresultN1[i].getField("NombrePlan__c"));
							planSitio.put("idCot", (String)  qresultN1[i].getField("Cotizacion__c"));
							for(int j = 0; j < qresultN2.length; j++) {
								if( ((String)qresultN1[i].getField("Id")).equals( ( (String)qresultN2[j].getField("BCot_SitioPlan__c") ) ) )  {
									planSitio.put("idOS", (String)  qresultN2[j].getField("Name"));
								}
							}
							listSitiosCot.add(planSitio);
						}
					}
				}else {
					if (qresultN1.length > 0) {
						if (qresultN1[0].getType().equals("Cot_SitioPlan__c")) {
							for (int i = 0; i < qresultN1.length; i++) {	
								Map<String,Object> planSitio = new HashMap<String, Object>();
								planSitio.put("nommbre", (String)  qresultN1[i].getField("NombrePlan__c"));
								planSitio.put("idCot", (String)  qresultN1[i].getField("Cotizacion__c"));
								listSitiosCot.add(planSitio);
							}
						}
					}
				}
			}else {
				if (qresultN1.length > 0) {
					if (qresultN1[0].getType().equals("Cot_SitioPlan__c")) {
						for (int i = 0; i < qresultN1.length; i++) {	
							Map<String,Object> planSitio = new HashMap<String, Object>();
							planSitio.put("nommbre", (String)  qresultN1[i].getField("NombrePlan__c"));
							planSitio.put("idCot", (String)  qresultN1[i].getField("Cotizacion__c"));
							listSitiosCot.add(planSitio);
						}
					}
				}
			}
		}
		response.setMensaje("Lista  Planes Cot");
		response.setResult(listSitiosCot);
		response.setSuccess(true);
		return response;
	}
	
}
