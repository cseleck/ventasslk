package com.mx.totalplay.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.totalplay.dao.ws.ClientSFSOAWSDAO;
import com.mx.totalplay.service.IMesaControlService;
import com.mx.totalplay.vo.ServiceResponse;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;


@Service
public class MesaControlImpl implements IMesaControlService{
	
	private static final Logger logger = LogManager.getLogger(MesaControlImpl.class);

	@Autowired
	private ClientSFSOAWSDAO clientSFSOAWSDAO;
	
	@Override
	public ServiceResponse getUltimasCotsMC() {
		logger.info("Service getUltimasCotsMC()");
		ServiceResponse response = new ServiceResponse();
		List<Object> listaCots = new ArrayList<Object>();
		String queryN1 = 
						    "SELECT Id, Name, Nombre__c, FORMAT(TSSolicitudMesaControl__c) "
						  + "FROM Cotizacion__c "
						  + "WHERE Estatus__c = 'Completada' AND SolicitarApoyoMesa__c = true "
						  + "AND Oportunidad__r.StageName = 'Cr�dito' AND CotizacionPrincipal__c = true " 
						  + "AND TSSolicitudMesaControl__c > 1000-01-01T00:00:00.000Z " 
						  + "ORDER BY TSSolicitudMesaControl__c ASC " 
						  + "LIMIT 10 ";
		
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("Cotizacion__c")) {
					for (int i = 0; i < qresultN1.length; i++) {
						Map<String,Object> cotMesa = new HashMap<String, Object>();
						cotMesa.put("idCot", 		 (String)qresultN1[i].getField("Id"));
						cotMesa.put("folioCot", 	 (String)qresultN1[i].getField("Name"));
						cotMesa.put("nombreCot", 	 (String)qresultN1[i].getField("Nombre__c"));
						SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
					    Integer diferencia = minutesDiff(GetItemDate((String)qresultN1[i].getField("TSSolicitudMesaControl__c")), GetItemDate(format.format(new Date())));
					    System.out.println(diferencia);
					    if(diferencia <= 5) {
					    	cotMesa.put("semaforo", "#11CA1F");
					    }else if(diferencia > 5 && diferencia <= 10) {
					    	cotMesa.put("semaforo", "#FFF700");
					    }else{
					    	cotMesa.put("semaforo", "#CC3F21");
					    }
					    listaCots.add(cotMesa);
					}
				}
			}
		}
		response.setMensaje("Ultimas Cots MC");
		response.setResult(listaCots);
		response.setSuccess(true);
		return response;
	}
	
	@Override
	public ServiceResponse getListCotsMC() {
		logger.info("Service getUltimasCotsMC()");
		ServiceResponse response = new ServiceResponse();
		List<Object> listaCots = new ArrayList<Object>();
		String queryN1 = 
						    "SELECT Id, Name, Nombre__c, FORMAT(TSSolicitudMesaControl__c), Oportunidad__r.Name, Oportunidad__r.Account.RazonSocial__c "
						  + "FROM Cotizacion__c "
						  + "WHERE Estatus__c = 'Completada' AND SolicitarApoyoMesa__c = true "
						  + "AND Oportunidad__r.StageName = 'Cr�dito' AND CotizacionPrincipal__c = true " 
						  + "AND TSSolicitudMesaControl__c > 1000-01-01T00:00:00.000Z " 
						  + "ORDER BY TSSolicitudMesaControl__c ASC ";
		
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("Cotizacion__c")) {
					for (int i = 0; i < qresultN1.length; i++) {
						Map<String,Object> cotMesa = new HashMap<String, Object>();
						cotMesa.put("idCot", 		 (String)qresultN1[i].getField("Id"));
						cotMesa.put("folioCot", 	 (String)qresultN1[i].getField("Name"));
						cotMesa.put("nombreCot", 	 (String)qresultN1[i].getField("Nombre__c"));
						if(null != qresultN1[i].getChild("Oportunidad__r")) {
							cotMesa.put("oportunidad", 	 (String)qresultN1[i].getChild("Oportunidad__r").getField("Name"));
							if(null != qresultN1[i].getChild("Oportunidad__r").getChild("Account")) {
								cotMesa.put("razonSocial", 	 (String)qresultN1[i].getChild("Oportunidad__r").getChild("Account").getField("RazonSocial__c"));
							}
						}
						SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
					    Integer diferencia = minutesDiff(GetItemDate((String)qresultN1[i].getField("TSSolicitudMesaControl__c")), GetItemDate(format.format(new Date())));
					    System.out.println(diferencia);
					    if(diferencia <= 5) {
					    	cotMesa.put("semaforo", "#11CA1F");
					    }else if(diferencia > 5 && diferencia <= 10) {
					    	cotMesa.put("semaforo", "#FFF700");
					    }else{
					    	cotMesa.put("semaforo", "#CC3F21");
					    }
					    listaCots.add(cotMesa);
					}
				}
			}
		}
		response.setMensaje("List Cots MC");
		response.setResult(listaCots);
		response.setSuccess(true);
		return response;
	}
	
	
	@Override
	public ServiceResponse getListDocsCot(String idCot) {
		logger.info("Service getUltimasCotsMC()");
		ServiceResponse response = new ServiceResponse();
		List<Object> listDocsCot = new ArrayList<Object>();
		String queryN1 = 
						      "Select ContentDocument.LatestPublishedVersion.Title, ContentDocument.LatestPublishedVersion.Id, "
						    		+ "ContentDocument.LatestPublishedVersion.FileType, ContentDocument.LatestPublishedVersion.FileExtension " 
						    + "from ContentDocumentLink " 
						    + "where LinkedEntityId = '" + idCot +  "'";
		
		QueryResult comentariosN1 = clientSFSOAWSDAO.query(queryN1);
		if (comentariosN1.getDone()) {
			SObject[] qresultN1 = comentariosN1.getRecords();
			if (qresultN1.length > 0) {
				if (qresultN1[0].getType().equals("ContentDocumentLink")) {
					for (int i = 0; i < qresultN1.length; i++) {
						if(null != qresultN1[i].getChild("ContentDocument")) {
							if(null != qresultN1[i].getChild("ContentDocument").getChild("LatestPublishedVersion")) {
								Map<String,Object> docCot = new HashMap<String, Object>();
								docCot.put("id", 	 qresultN1[i].getChild("ContentDocument").getChild("LatestPublishedVersion").getField("Id"));
								docCot.put("nombre", 	 qresultN1[i].getChild("ContentDocument").getChild("LatestPublishedVersion").getField("Title"));
								docCot.put("tipo", 	 qresultN1[i].getChild("ContentDocument").getChild("LatestPublishedVersion").getField("FileType"));
								docCot.put("extencion", 	 qresultN1[i].getChild("ContentDocument").getChild("LatestPublishedVersion").getField("FileExtension"));
								listDocsCot.add(docCot);
							}
						}
					}
				}
			}
		}
		response.setMensaje("List Cots MC");
		response.setResult(listDocsCot);
		response.setSuccess(true);
		return response;
	}
	
	
	@Override
	public ServiceResponse updateAprobarMesa(String idCot, String idOportunidad,String folioCredito,
			Boolean caratulaContrato, Boolean clausulado,
			Boolean rFC, Boolean buroCredito,
			Boolean actaConstitutiva, Boolean estadosCuenta,
			Boolean comprobanteDomicilio, Boolean identificacionOficial,
			Boolean consentimientoVista, Boolean tryAndBuy,
			String respuesta,String motivo,String comentarios) {

		ServiceResponse response = new ServiceResponse();
		
		SObject Opportunity = new SObject();
		Opportunity.setType("Opportunity");
		Opportunity.setField("Id", idOportunidad);
		
		Opportunity.setField("CaratulaContrato__c", caratulaContrato);
		Opportunity.setField("Clausulado__c", clausulado);
		Opportunity.setField("RFC__c", rFC);
		Opportunity.setField("BuroCredito__c", buroCredito);
		Opportunity.setField("ActaConstitutiva__c", actaConstitutiva);
		Opportunity.setField("EstadosCuenta__c", estadosCuenta);
		Opportunity.setField("ComprobanteDomicilio__c", comprobanteDomicilio);
		Opportunity.setField("IdentificacionOficial__c", identificacionOficial);
		Opportunity.setField("ConsentimientoVista__c", consentimientoVista);
		Opportunity.setField("Try_and_Buy__c", tryAndBuy);
		Opportunity.setField("EstatusMC__c", respuesta);
		
		if(!respuesta.equals("Pendiente")) {
			
			Opportunity.setField("FolioCredito__c", folioCredito);
			Opportunity.setField("MotivoRechazoMC__c", motivo);
			Opportunity.setField("ObservacionesAnte_MC__c", comentarios);
			
			if(respuesta.equals("Validada")) {
				Opportunity.setField("ValidacionMesa__c", true);
				Opportunity.setField("StageName", "Ganada");
				Opportunity.setField("RechazoAnte_MC__c", false);
			}else if(respuesta.equals("Rechazada")) {
				Opportunity.setField("ValidacionMesa__c", false);
				Opportunity.setField("StageName", "Perdida");
				Opportunity.setField("RechazoAnte_MC__c", true);
			}
			
		}
		SaveResult[] Update = clientSFSOAWSDAO.update(new SObject[] { Opportunity });
		Boolean updateOportunidad = false;
		for (int z = 0; z < Update.length; z++) {
			if (Update[z].isSuccess()) {
				logger.info("Se Actualizo la Oportunidad : " + Update[z].getId());
				updateOportunidad=true;
			}else {
				response.setMensaje("Error al Guardar la Oportunidad");
				response.setSuccess(false);
			}
		}
		if(updateOportunidad == true && !respuesta.equals("Pendiente")) {
			SObject Cotizacion__c = new SObject();
			Cotizacion__c.setType("Cotizacion__c");
			Cotizacion__c.setField("Id", idCot);
			Cotizacion__c.setField("Estatus__c", "Cerrada");
			
			SaveResult[] UpdateCot = clientSFSOAWSDAO.update(new SObject[] { Cotizacion__c });
			for (int z = 0; z < UpdateCot.length; z++) {
				if (UpdateCot[z].isSuccess()) {
					logger.info("Se Actualizo la Cotizacion : " + UpdateCot[z].getId());
				}else {
					response.setMensaje("Error al Guardar la Cotizacion");
					response.setSuccess(false);
				}
			}
		}
		response.setMensaje("Opotunidad Guardada con exito");
		response.setSuccess(true);
		return response;
	}
			
	public Date GetItemDate(final String date){
	    final Calendar cal = Calendar.getInstance(TimeZone.getDefault());
	    final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
	    format.setCalendar(cal);
	    try {
	        return format.parse(date);
	    } catch (Exception e) {
	    	logger.error(e.getMessage());
	    	return null;
	    }
	}
	
	public Integer minutesDiff(Date earlierDate, Date laterDate){
	    if( earlierDate == null || laterDate == null ) return 0;
	    return (int)((laterDate.getTime()/60000) - (earlierDate.getTime()/60000));
	}
	
}
