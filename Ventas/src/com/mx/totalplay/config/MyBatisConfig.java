package com.mx.totalplay.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mx.totalplay.dao.LogDAO;
import com.mx.totalplay.dao.SFDAO;
import com.mx.totalplay.dao.UsuariosDAO;
import com.mx.totalplay.dao.ws.ClientRestWSDAO;
import com.mx.totalplay.dao.ws.ClientSFSOAWSDAO;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
public class MyBatisConfig {

	@Bean(name = "dataSource")
	@Primary
	public DataSource dataSource() {
		JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
		jndiObjectFactoryBean.setJndiName("jdbc/KVW_DB");
		jndiObjectFactoryBean.setResourceRef(true);
		jndiObjectFactoryBean.setProxyInterface(DataSource.class);
		try {
			jndiObjectFactoryBean.afterPropertiesSet();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (NamingException e) {
			e.printStackTrace();
			return null;
		}
		return (DataSource) jndiObjectFactoryBean.getObject();
	}

	@Bean
	public DataSourceTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}
	
	@Bean(name = "sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(dataSource());
		factoryBean.setMapperLocations(
				new PathMatchingResourcePatternResolver().getResources("classpath:com/mx/totalplay/mapper/*.xml"));
		return factoryBean.getObject();
	}
	
	@Bean
	public LogDAO logDAO() {
		return new LogDAO();
	}
	
	@Bean
	public UsuariosDAO usuariosDAO() {
		return new UsuariosDAO();
	}
	
	@Bean
	public ClientRestWSDAO clientRestWSDAO() {
		return new ClientRestWSDAO();
	}
	
	@Bean
	public ClientSFSOAWSDAO clientSFSOAWSDAO() {
		return new ClientSFSOAWSDAO();
	}
	
	@Bean
	public SFDAO sfdao() {
		return new SFDAO();
	}
	
	
	 @Scheduled
	 public void demoServiceMethod()
	    {
	        System.out.println("Method executed at every 5 seconds. Current time is :: ");
	    }
}
