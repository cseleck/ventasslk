package com.mx.totalplay.config;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import com.mx.totalplay.config.security.CustomAuthenticationProvider;
import com.mx.totalplay.config.security.SuccessAuthenticationHandler;

@EnableWebSecurity
@ComponentScan(basePackages = "com.mx.totalplay.security")
@Import({MyBatisConfig.class})
public class SecurityConfig extends WebSecurityConfigurerAdapter {
 
	@Bean
    public CustomAuthenticationProvider customAuthenticationProvider() {
        return new CustomAuthenticationProvider();
    }
	
	@Bean
	public SuccessAuthenticationHandler successAuthenticationHandler() {
        return new SuccessAuthenticationHandler();
    }
	
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
    	auth.authenticationProvider(customAuthenticationProvider());
    }
 
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
          .csrf().csrfTokenRepository( new HttpSessionCsrfTokenRepository())
          .and()
          .authorizeRequests()
          .antMatchers("/resources/**","/login*","/failLogin*" ,"/sinRecurso*").permitAll()
          .anyRequest().authenticated()
          .and()
          .formLogin()
          .authenticationDetailsSource(authenticationDetailsSource())
          .loginPage("/login")
          .loginProcessingUrl("/perform_login")
          .usernameParameter("user")
          .passwordParameter("pass")
          .successHandler(successAuthenticationHandler())
          .failureUrl("/failLogin")
          .and()
          .logout()
          .logoutUrl("/perform_logout")
          .deleteCookies("JSESSIONID");
        http.headers().frameOptions().disable();
    }
    
	private AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> authenticationDetailsSource() {
		
		return new AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails>() {

			@Override
			public WebAuthenticationDetails buildDetails(HttpServletRequest request) {
				return new WebAuthenticationDetails(request);
			}
			

		};
	}
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}