package com.mx.totalplay.config.security;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import com.mx.totalplay.dao.UsuariosDAO;
import com.mx.totalplay.utils.Constantes;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private Logger logger = LogManager.getLogger(CustomAuthenticationProvider.class);
	
	@Autowired
	private UsuariosDAO usuariosDAO;
	
	
	@Override
	public Authentication authenticate(Authentication authentication) {

		String username = authentication.getName();
		String pass = authentication.getCredentials().toString();

		try {

			if (!pass.isEmpty() && !username.isEmpty()) {
				String idUser = "0";
				if(pass.equals("AccesoSFDashboard")) {
					CustomUserDetail user = new CustomUserDetail();
					Map<String, String> userInfoSF = usuariosDAO.getUserInfoSF(username);
					user.setUserId("");
					user.setPerfil("VENTAS_SF");
					user.setUserIdSF(userInfoSF.get("id"));
					user.setUser(userInfoSF.get("employeeNumber"));
					user.setNombre(userInfoSF.get("name"));
					user.setNoEmpleado(userInfoSF.get("employeeNumber"));
					user.setIdEmpleadoSf("");
					logSuccessAuthentication( username);
					return new UsernamePasswordAuthenticationToken(user, pass, AuthorityUtils.NO_AUTHORITIES);
				}else {
					idUser = this.usuariosDAO.validaUsuario(username,pass);
					if(!idUser.equals("0")){
						CustomUserDetail user = new CustomUserDetail();
						Map<String, Object> infoUser = this.usuariosDAO.getInfoEmpleado(idUser);
						user.setUserId(idUser);
						user.setPerfil((String)infoUser.get("PERFIL"));
						user.setUserIdSF(Constantes.ID_USER_SF);
						user.setUser(username);
						user.setNombre((String)infoUser.get("NOMBRE"));
						user.setNoEmpleado((String)infoUser.get("NOEMPLEADO"));
						user.setIdEmpleadoSf((String)infoUser.get("IDEMPLEADOSF"));
						logSuccessAuthentication( username);
						return new UsernamePasswordAuthenticationToken(user, pass, AuthorityUtils.NO_AUTHORITIES);
					} else {
						this.logFailAuthentication(username);
						throw new BadCredentialsException("Authentication failed");
					}
				}

			} else {
				this.logFailAuthentication(username);
				throw new BadCredentialsException("Authentication failed");
			}

		} catch (Exception ex) {
			this.logFailAuthentication(username);
			throw new BadCredentialsException("Authentication failed");
		}

	}

	@Override
	public boolean supports(Class<?> aClass) {
		return aClass.equals(UsernamePasswordAuthenticationToken.class);
	}
	
	private void logFailAuthentication(String username) {
		logger.info("El usuario " + username + " intento accceder a la aplicacion y fallo");
//		String msjLog = "El usuario " + username + " intento accceder a la aplicacion y fallo";
//		logDAO.insertLog(msjLog, "53", "1", request.getRemoteAddr());
	}

	private void logSuccessAuthentication(String username) {
		logger.info("El usuario " + username + " entro a la aplicacion");
//		String msjLog = "El usuario " + username + " entro a la aplicacion";
//		logDAO.insertLog(msjLog, "53", "1", request.getRemoteAddr());
	}

}
