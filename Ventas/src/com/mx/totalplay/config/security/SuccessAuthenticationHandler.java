package com.mx.totalplay.config.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class SuccessAuthenticationHandler implements AuthenticationSuccessHandler {

	protected final Log logger = LogFactory.getLog(this.getClass());

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	public SuccessAuthenticationHandler() {
		super();
	}

	@Override
	public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException {
		clearAuthenticationAttributes(request);
		handle(request, response, authentication);
	}

	protected void handle(HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException {
		final String targetUrl = determineTargetUrl(authentication, request);

		if (response.isCommitted()) {
			logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
			return;
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	protected String determineTargetUrl(Authentication authentication, final HttpServletRequest request) {

		HttpSession session = request.getSession(true);
		CustomUserDetail userDetail = (CustomUserDetail) authentication.getPrincipal();
		this.setUserSessionValues(session, userDetail);
		if(userDetail.getPerfil().equals("VENTAS_SF")) {
			return "/DashboardSF";
		}else if(userDetail.getPerfil().equals("MESA_CONTROL")){
			return "/DashboardMesa";
		}else {
			return "/close";
		}

	}

	/**
	 * Removes temporary authentication-related data which may have been stored in
	 * the session during the authentication process.
	 */
	protected final void clearAuthenticationAttributes(final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);

		if (session == null) {
			return;
		}

		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}

	public void setRedirectStrategy(final RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

	private void setUserSessionValues(HttpSession session, CustomUserDetail userDetail) {
		session.setAttribute("userId",userDetail.getUserId());
		session.setAttribute("perfil", userDetail.getPerfil());
		session.setAttribute("userIdSF", userDetail.getUserIdSF());
		session.setAttribute("user", userDetail.getUser());
		session.setAttribute("nombre", userDetail.getNombre());
		session.setAttribute("noEmpleado", userDetail.getNoEmpleado());
		session.setAttribute("idEmpleadoSf", userDetail.getIdEmpleadoSf());
	}

}