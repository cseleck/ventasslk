package com.mx.totalplay.config.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetail implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5333622885462834963L;
	
	private String userId;
	private String perfil;
	private String userIdSF;
	private String user;
	private String nombre;
	private String noEmpleado;
	private String idEmpleadoSf;

	/* Spring Security related fields */
	private List<Role> authorities;
	private boolean accountNonExpired = true;
	private boolean accountNonLocked = true;
	private boolean credentialsNonExpired = true;
	private boolean enabled = true;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public String getUsername() {
		return user;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public void setAuthorities(List<Role> authorities) {
		this.authorities = authorities;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getNoEmpleado() {
		return noEmpleado;
	}

	public void setNoEmpleado(String noEmpleado) {
		this.noEmpleado = noEmpleado;
	}

	public String getUserIdSF() {
		return userIdSF;
	}

	public void setUserIdSF(String userIdSF) {
		this.userIdSF = userIdSF;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getIdEmpleadoSf() {
		return idEmpleadoSf;
	}

	public void setIdEmpleadoSf(String idEmpleadoSf) {
		this.idEmpleadoSf = idEmpleadoSf;
	}

//	private List<CatalogoVO> listEdosReports;
//	private String latitud;
//	private String longitud;
//	private String zoom;
//	private String tipoUsuario;
//	private OperationsVO operationsVO;
//	private String extAvaya;
//	private String idDespachador;
//	private String pais;
//	private String fechaActual;
//	private List<AccesosUSRVO> accesos;
	
//	public List<CatalogoVO> getListEdosReports() {
//		return listEdosReports;
//	}
//
//	public void setListEdosReports(List<CatalogoVO> listEdosReports) {
//		this.listEdosReports = listEdosReports;
//	}
//
//	public String getLatitud() {
//		return latitud;
//	}
//
//	public void setLatitud(String latitud) {
//		this.latitud = latitud;
//	}
//
//	public String getLongitud() {
//		return longitud;
//	}
//
//	public void setLongitud(String longitud) {
//		this.longitud = longitud;
//	}
//
//	public String getZoom() {
//		return zoom;
//	}
//
//	public void setZoom(String zoom) {
//		this.zoom = zoom;
//	}
//	public String getTipoUsuario() {
//		return tipoUsuario;
//	}
//
//	public void setTipoUsuario(String tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}
//
//	public OperationsVO getOperationsVO() {
//		return operationsVO;
//	}
//
//	public void setOperationsVO(OperationsVO operationsVO) {
//		this.operationsVO = operationsVO;
//	}
//
//	public String getExtAvaya() {
//		return extAvaya;
//	}
//
//	public void setExtAvaya(String extAvaya) {
//		this.extAvaya = extAvaya;
//	}
//
//	public String getIdDespachador() {
//		return idDespachador;
//	}
//
//	public void setIdDespachador(String idDespachador) {
//		this.idDespachador = idDespachador;
//	}
//
//	public String getPais() {
//		return pais;
//	}
//
//	public void setPais(String pais) {
//		this.pais = pais;
//	}
//
//	public String getFechaActual() {
//		return fechaActual;
//	}
//
//	public void setFechaActual(String fechaActual) {
//		this.fechaActual = fechaActual;
//	}
//
//	public List<AccesosUSRVO> getAccesos() {
//		return accesos;
//	}
//
//	public void setAccesos(List<AccesosUSRVO> accesos) {
//		this.accesos = accesos;
//	}

	

}
