package com.mx.totalplay.vo;

public class InfoUserVO {
	
    private String UserType;
    private String Name;
    private String TipoC;
    private String UserRollID;
    private String NoEmpledoC;
    private String AccountId;
    private String Email;
    private String GrupoC;
    private String IsActive;
    private String Id;
    private String LastName;
    private String GrupoAsignadoC;
    
	public String getUserType() {
		return UserType;
	}
	public void setUserType(String userType) {
		UserType = userType;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getTipoC() {
		return TipoC;
	}
	public void setTipoC(String tipoC) {
		TipoC = tipoC;
	}
	public String getUserRollID() {
		return UserRollID;
	}
	public void setUserRollID(String userRollID) {
		UserRollID = userRollID;
	}
	public String getNoEmpledoC() {
		return NoEmpledoC;
	}
	public void setNoEmpledoC(String noEmpledoC) {
		NoEmpledoC = noEmpledoC;
	}
	public String getAccountId() {
		return AccountId;
	}
	public void setAccountId(String accountId) {
		AccountId = accountId;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getGrupoC() {
		return GrupoC;
	}
	public void setGrupoC(String grupoC) {
		GrupoC = grupoC;
	}
	public String getIsActive() {
		return IsActive;
	}
	public void setIsActive(String isActive) {
		IsActive = isActive;
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getGrupoAsignadoC() {
		return GrupoAsignadoC;
	}
	public void setGrupoAsignadoC(String grupoAsignadoC) {
		GrupoAsignadoC = grupoAsignadoC;
	}
    
}
