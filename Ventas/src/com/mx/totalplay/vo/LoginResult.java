package com.mx.totalplay.vo;

public class LoginResult {

	    private ResponseVO Response;
	    private InfoUserVO InfoUser;
	    
		public ResponseVO getResponse() {
			return Response;
		}
		public void setResponse(ResponseVO response) {
			Response = response;
		}
		public InfoUserVO getInfoUser() {
			return InfoUser;
		}
		public void setInfoUser(InfoUserVO infoUser) {
			InfoUser = infoUser;
		}
	    
}
