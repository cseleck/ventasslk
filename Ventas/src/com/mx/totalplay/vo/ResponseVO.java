package com.mx.totalplay.vo;

public class ResponseVO {
    private String Result;
    private String ResultId;
    private String Description;
    
	public String getResult() {
		return Result;
	}
	public void setResult(String result) {
		Result = result;
	}
	public String getResultId() {
		return ResultId;
	}
	public void setResultId(String resultId) {
		ResultId = resultId;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
    
}
