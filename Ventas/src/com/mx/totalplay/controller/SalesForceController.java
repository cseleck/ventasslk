package com.mx.totalplay.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mx.totalplay.service.ISalesForceService;
import com.mx.totalplay.vo.ServiceResponse;

@RestController
public class SalesForceController {
	@Autowired
	private ISalesForceService salesForceService;
	
	@PostMapping(value = "/getChatterCot")
	public ServiceResponse getChatterCot(String idCot) {
		return this.salesForceService.getChatterCot(idCot);
	}
	
	@PostMapping(value = "/getFileContentVersion")
	public ServiceResponse getFileChatterCot(String idFile) {
		return this.salesForceService.getFileContentVersion(idFile);
	}
	
	@PostMapping("/insertChartterCot")
	@ResponseBody
	public ServiceResponse insertChartterCot(HttpSession session, MultipartFile file, String idParent, String textCharter,String nivel) {
		ServiceResponse response = new ServiceResponse();
		String idEmpleado = (String) session.getAttribute("userIdSF");
		String nombreEmpleado = (String)session.getAttribute("noEmpleado") + "_" + (String)session.getAttribute("nombre") + " @@ " ;
		if ((textCharter == null || textCharter.equals("")) && (file == null || file.getOriginalFilename() == null || file.getOriginalFilename().equals(""))) {
			response.setMensaje("Debe agregar un comentario o adjuntar un archivo");
			response.setSuccess(false);
		} else {
			Float tamanoFile = (float) (file.getSize() / 1024) / 1024;
			if (file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")) {
				if (tamanoFile > 25.6) {
					response.setSuccess(false);
					response.setMensaje("El tama�o del archivo no debe ser mayor a 25.6 Mb");
					return response;
				}
			}
			response = this.salesForceService.insertChartterCot(file, idParent, (nombreEmpleado + textCharter), idEmpleado, nivel);
		}
		return response;
	}
	
	@PostMapping("/getCotsVendedor")
	@ResponseBody
	public ServiceResponse getCotsVendedor(HttpSession session) {
		ServiceResponse response = new ServiceResponse();
		String idEmpleado = (String) session.getAttribute("userIdSF");
		response = this.salesForceService.getCotsVendedor(idEmpleado);
		return response;		
	}
	
	@PostMapping("/getDetalleCot")
	@ResponseBody
	public ServiceResponse getDetalleCot(String idCot) {
		ServiceResponse response = new ServiceResponse();
		response = this.salesForceService.getDetalleCot(idCot);
		return response;		
	}
	
	@PostMapping("/getSitiosCot")
	@ResponseBody
	public ServiceResponse getSitiosCot(String idCot) {
		ServiceResponse response = new ServiceResponse();
		response = this.salesForceService.getSitiosCot(idCot);
		return response;		
	}
	
	@PostMapping("/getPlanesSitiosCot")
	@ResponseBody
	public ServiceResponse getPlanesSitiosCot(String idSitio) {
		ServiceResponse response = new ServiceResponse();
		response = this.salesForceService.getPlanesSitiosCot(idSitio);
		return response;		
	}
	
	@PostMapping("/getHistorialSitio")
	@ResponseBody
	public ServiceResponse getHistorialSitio(String idOS, String idCot) {
		ServiceResponse response = new ServiceResponse();
		Map<String,Object> historial = new HashMap<String, Object>();
		historial.put("mesa", this.salesForceService.getEstatusSitio(idCot));
		if(!(null == idOS || idOS.isEmpty())) {
			response = this.salesForceService.getHistorialSitio(idOS);
			if(response.isSuccess()) {
				historial.put("sitio", response.getResult());
			}
		}
		response.setSuccess(true);
		response.setResult(historial);
		return response;		
	}
	
	
}
