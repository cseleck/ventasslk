package com.mx.totalplay.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogInController {

//	private static final Logger log = LogManager.getLogger(LogInController.class);

	@GetMapping(value = "/login")
	public ModelAndView login() {
		System.out.println("-----primero-------------------------------------------");
		ModelAndView login = new ModelAndView("/loginCustom");  
//		login.addObject("version", Constantes.VERSION);
		return login;
	}
	
	@GetMapping(value = "/logout")
    public String logout(SessionStatus session) {
        SecurityContextHolder.getContext().setAuthentication(null);
        session.setComplete();
        return "redirect:/login";
    }
	
	@GetMapping(value = "/failLogin")
	public ModelAndView faillogin() {
		return new ModelAndView("/failLogin");
	}
    
}
