package com.mx.totalplay.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * Controlador exclusivo para rutas de vistas
 * **/
@Controller
public class MainController {
	

	@GetMapping(value = { "/", "", "/home" })
	public String getWelcomePage() {
		return "login";
	}

	@GetMapping(value = "/error")
	public ModelAndView error() {
		return new ModelAndView("/Generic/403");
	}

	@GetMapping(value = "/DashboardSF")
	public ModelAndView indicadores() {
		return new ModelAndView("/dashboardsf/dashboardSF");
	}
	
	@GetMapping(value = "/DashboardMesa")
	public ModelAndView DashboardMesa() {
		return new ModelAndView("/mesaControl/dashboardMesa");
	}
	
	@GetMapping(value = "/close")
	public ModelAndView close() {
		return new ModelAndView("/close");
	}
	
	@GetMapping(value = "/dashboardUser")
	public ModelAndView dashboardUser() {
		return new ModelAndView("/adminUser/dashboardUser");
	}
}
