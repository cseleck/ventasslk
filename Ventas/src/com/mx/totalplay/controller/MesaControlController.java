package com.mx.totalplay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.totalplay.service.IMesaControlService;
import com.mx.totalplay.vo.ServiceResponse;

@RestController
public class MesaControlController {
	@Autowired
	
	private IMesaControlService mesaControlService;
	
	@PostMapping(value = "/getUltimasCotsMC")
	public ServiceResponse getUltimasCotsMC() {
		return this.mesaControlService.getUltimasCotsMC();
	}
	
	@PostMapping(value = "/getListCotsMC")
	public ServiceResponse getListCotsMC() {
		return this.mesaControlService.getListCotsMC();
	}
	
	@PostMapping(value = "/getListDocsCot")
	public ServiceResponse getListDocsCot(String idCot) {
		return this.mesaControlService.getListDocsCot(idCot);
	}
	
	@PostMapping(value = "/updateAprobarMesa")
	public ServiceResponse updateAprobarMesa(String idCot, String idOportunidad, String folioCredito, Boolean caratulaContrato,
			Boolean clausulado, Boolean rFC, Boolean buroCredito, Boolean actaConstitutiva, Boolean estadosCuenta,
			Boolean comprobanteDomicilio, Boolean identificacionOficial, Boolean consentimientoVista, Boolean tryAndBuy,
			String respuesta, String motivo, String comentarios) {
		
		return this.mesaControlService.updateAprobarMesa(idCot, idOportunidad, folioCredito, caratulaContrato, clausulado, rFC, 
				buroCredito, actaConstitutiva, estadosCuenta, comprobanteDomicilio, identificacionOficial, consentimientoVista, 
				tryAndBuy, respuesta, motivo, comentarios);
		
	}

}
