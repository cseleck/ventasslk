const json_swal= {background: '#00000000',showCancelButton: false,showConfirmButton: false,imageUrl: 'resources/img/login/2.gif'};
var table=null;
$(document).ready(function(){

	swal(json_swal);
	
	
	$(document).ajaxSend(function(e, xhr, options) {
	    xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"),$("meta[name='_csrf']").attr("content"));
	});
	
	
	 
	 
	 setTimeout(function(){
		 init();
   }, 1000);
	
//	init();
})

function init(){
	
	table=$('#example').DataTable({
		"lengthChange": false,
		 "lengthMenu":[16],
		 "order": [[ 1, "desc" ]],
		columnDefs:[            
			{
	            "targets": [ 0 ],
	            "visible": false,
	            "searchable": false
	        },
			{
				 "className": "dt-center",
				 "targets":[7]
				}
        ],
		"language" : {
			"sProcessing" : "Procesando...",
			"sLengthMenu" : "Mostrar _MENU_ registros",
			"sZeroRecords" : "No se encontraron resultados",
			"sEmptyTable" : "Ning\u00fana OT disponible ",
			"sInfo" : "",
			"sInfoEmpty" : "",
			"sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix" : "",
			"sSearch" : "Buscar:",
			"sUrl" : "",
			"sInfoThousands" : ",",
			"sLoadingRecords" : "<br/><br/>Cargando...<br/><br/>",
			"oPaginate" : {
				"sFirst" : "Primero",
				"sLast" : "\u00daltimo",
				"sNext" : "Siguiente",
				"sPrevious" : "Anterior"
			},
			"oAria" : {
				"sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending" : ": Activar para ordenar la columna de manera descendente"
			},
			"rowReorder" : true
		}
	});
	initTabla();
	

}

//* INICIO CREA TABLA PRINCIPAL*//

function returnTable(){
	initTabla();
	$('#divTablaCots').show();
	$('#divDetalleCot').hide();

}

function initTabla(){
	getCotsVendedor();
}

function cargaDetalle(idCot){
	seguimientoSitio.addStyleButtonSeguimientoSitio()
    dashboardSF.getDetalleCot(idCot);
    dashboardSF.getSitiosCotizacion(idCot);
    dashboardSF.getChatterCot(idCot);
    $('#idCotChatter').val(idCot);

}

function getCotsVendedor() {
	table.clear().draw();
	dashboardSF.getCotsVendedor();
}

function nuevoApuntador(data){
	
	swal(json_swal);
	setTimeout(function() {
		$.ajax({
			url : cargaDetalle(data),
			async: false,
			type : "POST",
			dataType : "json",
			complete : function(jqXHR, textStatus) {
			    $('#divTablaCots').hide();
			    $('#divDetalleCot').show();
				swal.close();
			},
			error : function(jqXHR, textStatus, errorThrown) {
				swal.close();
			}
		});
	}, 2500);
}

function agregarFilaTablaIdCot (data) {
	let id = data['id'];
    var valores = [];
    valores.push(data['id']);
    valores.push(data['name']);
    valores.push(data['nombre']);
    valores.push(data['estatus']);
    valores.push((undefined != data['oportunidad'] ? data['oportunidad'] : ' '));
    valores.push( (undefined != data['razonSocial'] ? data['razonSocial'] : ' '));
    valores.push(data['fecha']);
    valores.push('<a  data-toggle="tooltip" title="Ver Detalle" href="#" onclick="nuevoApuntador(\''+id+'\')" >&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-chevron-right" style="cursor: pointer; color:white;" ></i>&nbsp;&nbsp;&nbsp;&nbsp;</a>');
    valores.push(data);
    
    table.row.add(valores).draw(false);
}

//**FIN TABLA PRINCIPAL**//

//** INICIO INFORMACION DETALLE DE LA COT **///
var informacion={
		
	getDataCliente:function(result){
		
		// gral
		let folioCot  = informacion.validaDato(result.result.folioCot)
		let nombreCot = informacion.validaDato(result.result.nombreCot)
			
		// Account
		let nombreCta      = informacion.validaDato(result.result.nombreOpor)
		let razonSocCta    = informacion.validaDato(result.result.razonSocCta)
		let tipoPersonaCta = informacion.validaDato(result.result.tipoPersonaCta)
		let telCta         = informacion.validaDato(result.result.telCta)
		let sectorCta      = informacion.validaDato(result.result.sectorCta)
		let contactoCta    = informacion.validaDato(result.result.contactoCta)
			
		// Oportunidad__r
		let nombreOpor     = informacion.validaDato(result.result.nombreOpor)
		let NumeroOpor     = informacion.validaDato(result.result.NumeroOpor)
		let tipoOpor       = informacion.validaDato(result.result.tipoOpor)
		let subTipoOpor    = informacion.validaDato(result.result.subTipoOpor)
		let origenOpor     = informacion.validaDato(result.result.origenOpor)
		let segmentoOpor   = informacion.validaDato(result.result.segmentoOpor)
		let etapaOpor      = informacion.validaDato(result.result.etapaOpor)
		let probaOpor      = informacion.validaDato(result.result.probaOpor)
		
		$("#folioCot").text(folioCot)
		$("#headerSeguiSItio").text(folioCot)
		$("#nombreCot").text(nombreCot)	
		
		$("#nombreCta").text(nombreCta)
		$("#razonSocCta").text(razonSocCta)
		$("#tipoPersonaCta").text(tipoPersonaCta)
		$("#telCta").text(telCta)
		$("#sectorCta").text(sectorCta)
		$("#contactoCta").text(contactoCta)
		
		$("#nombreOpor").text(nombreOpor)
		$("#NumeroOpor").text(NumeroOpor)
		$("#tipoOpor").text(tipoOpor)
		$("#subTipoOpor").text(subTipoOpor)
		$("#origenOpor").text(origenOpor)
		$("#segmentoOpor").text(segmentoOpor)
		$("#etapaOpor").text(etapaOpor)
		$("#probaOpor").text(probaOpor+'%')
	}, 
	validaDato:function(data){
		try {
				if(data ==undefined){
					return '---';
				}else{
					return data;
				}
			}
			catch(error) {
				return '---';
			}
		
	},
	
}
//** FIN INFORMACION DETALLE DE LA COT **///

//** INICIO LISTA SITIOS COT **//
var SitioCotizacion={
	tableSitioCot:function(data){
		$('#tableSitioCot').html('')
		
		let container = ''
		$.each(data.result, function( index, value ) {
			let nombre     = undefined == value.name ? '' : value.name ;
			let idCot      = undefined == value.idCot ? '' : value.idCot;
			let direccion  = undefined == value.direccion ? '' : value.direccion;
			let os 		   = undefined == value.idOs ? '' : value.idOs;
			let ot 		   = undefined == value.idOt ? '' : value.idOt;
			let sitio 	   = undefined == value.idSitio ? '' : value.idSitio;
			container +=''+
				  '<tr class="mdc-data-table__row" onclick="SitioCotizacion.addTableDelait(\'' + nombre + '\',\''+idCot+'\',\''+ot+'\',\''+os+'\',\''+direccion+'\',\''+sitio+'\')">'
				  	+ '<td class="mdc-data-table__cell" >'
				  		+ '<p style="margin: 1px;">'
				  			+ '<span class="headerInfo">'
				  				+ '<i class="iconMap fas fa-map-marker-alt"></i> ' + nombre + '  ' + os
				  			+ '</span>'
				  		+ '</p>'
				  		+ '<span class = "colorLetterGris"> ' + direccion + ' </span>'
				  	+ '</td>'
				+ '</tr>'
				+'<tr class="hidePOM" id="secondView'+nombre+'">'
				+'</tr>';
		});
		$('#tableSitioCot').html(container)
	},
	getDataSegSitio:function(nombre,idCot,ot){
		
		$("#headerSeguiSItio").text(nombre);
		dashboardSF.getHistorialSitio(ot,idCot)	
		
	},
	addTableDelait:function(nombre ,idCot,ot,os,direccion,sitio){
		dashboardSF.getPlanesSitiosCot(sitio, nombre)
	}, 
	addTableResultDelait:function(result, nombre){
		
		if(result.result.length == 0){
			//openLoader('Sin Planes','info')
			return;
		}
		$('#secondView'+nombre).html('')
			let container ='<td class="mdc-data-table__cell" >';
			
		
		$.each(result.result, function( index, value ) {
			
			let name = value.nommbre  = undefined == value.nommbre ? '' : value.nommbre;
			let idCot  = value.idCot    = undefined == value.idCot ? '' : value.idCot;
			let os     = value.idOS     = undefined == value.idOS ? '' : value.idOS;
			
			container +=
				
		  		'<p style="margin: 1em; cursor: pointer;" onclick="SitioCotizacion.getDataSegSitio(\'' + nombre + '\',\''+idCot+'\',\''+os+'\')">'
		  			+ '<span class="headerInfo">'
		  				+''+ name + ''
		  			+ '</span>'
		  		+ '</p>'
		  	;
		})  ;
		container += '</td>';
	$('#secondView'+nombre).html(container).removeClass('hidePOM').addClass('showPOM')
	}
}

//** FINT LISTA SITIOS COT **//

//** INICIO SEGUIMIENTO HISTORIAL SITIO COT **//
var seguimientoSitio={	
	addInfoToSeguimientoSitio:function(result){
		this.addStyleButtonSeguimientoSitio()
		if(result.result.mesa.mesa){
			
			let mc =seguimientoSitio.validaDataNUll(result.result.mesa.mesaDetalle)
			$("#spanMC").text(mc)
			seguimientoSitio.showDetailData(1)
		}
		
		if(result.result.sitio !=undefined){
			
			let fecha =seguimientoSitio.validaDataNUll(result.result.sitio.Agendamiento.Fecha)
			let turno =seguimientoSitio.validaDataNUll(result.result.sitio.Agendamiento.Turno)
			let servi =seguimientoSitio.validaDataNUll(result.result.sitio.Agendamiento.OS)
			
			$("#spanAGFecha").text(fecha)
			$("#spanAGTurno").text(turno)
			$("#spanAGOS").text(servi)
			
			seguimientoSitio.showDetailData(2)
			
			let tecnico =seguimientoSitio.validaDataNUll(result.result.sitio.Instalacion.Tecnico)
			let statusOT =seguimientoSitio.validaDataNUll(result.result.sitio.Instalacion.StatusOT)
			let auto =seguimientoSitio.validaDataNUll(result.result.sitio.Instalacion.Auto)
			
			$("#spanInsTecnico").text(tecnico)
			$("#spanInsAuto").text(auto)
			$("#spanInsSubStatus").text('7')
			$("#spanInsStatus").text(statusOT)
			
			seguimientoSitio.showDetailData(3)
			
			let cuenta =seguimientoSitio.validaDataNUll(result.result.sitio.Activacion.Cuenta)
			let fechaActivacion =seguimientoSitio.validaDataNUll(result.result.sitio.Activacion.FechaActivacion)
			
			$("#spanFacFecha").text(fechaActivacion)
			$("#spanFacTurno").text(cuenta)
			
			seguimientoSitio.showDetailData(4)

			
		}
	},
	showDetailData:function(type){
		let arreidDom = []
		switch (type) {
		case 1:
			arreidDom=[]
			arreidDom.push("containerMC")
			arreidDom.push("iconMC")
			arreidDom.push("tituloMC")
			arreidDom.push("mesacontrol")
				seguimientoSitio.estiloSeguimiento(arreidDom)
			
			break;
		case 2:
			
			arreidDom=[]
			arreidDom.push("containerAgen")
			arreidDom.push("iconAgen")
			arreidDom.push("tituloAgen")
			arreidDom.push("agendamiento")
				seguimientoSitio.estiloSeguimiento(arreidDom)
			break;
		case 3:
			arreidDom=[]
			arreidDom.push("containerIns")
			arreidDom.push("iconIns")
			arreidDom.push("tituloIns")
			arreidDom.push("instalacion")
				seguimientoSitio.estiloSeguimiento(arreidDom)
			break;
		case 4:
			arreidDom=[]
			arreidDom.push("containerFc")
			arreidDom.push("iconFc")
			arreidDom.push("tituloFc")
			arreidDom.push("facturacion")
				seguimientoSitio.estiloSeguimiento(arreidDom)
			break;

		default:
			break;
		}
	},
	estiloSeguimiento:function(arreglo){
		let container = arreglo[0]
		let icon = arreglo[1]
		let titulo = arreglo[2]
		let domData = arreglo[3]
		
		$("#"+container).removeClass('espaciodeSeguimientoSitio');
		if(container == "containerMC"){
			$("#"+container).addClass('espacioSeguimientoSitioInicial')
		}else{
			$("#"+container).addClass('espaciodeSeguimientoSitioExpandir')
		}
		$("#"+icon).removeClass('bgroundDesabilitarSeguimiento');
		$("#"+domData).removeClass('hidePOM');
		$("#"+domData).addClass('showPOM')
		$("#"+titulo).removeClass('colorDesabilitarSeguimiento')
		
	},addStyleButtonSeguimientoSitio:function(){
		arreMC=[]
		arreMC.push("containerMC")
		arreMC.push("iconMC")
		arreMC.push("tituloMC")
		arreMC.push("mesacontrol")
		
		arreAG=[]
		arreAG.push("containerAgen")
			arreAG.push("iconAgen")
			arreAG.push("tituloAgen")
			arreAG.push("agendamiento")
		
		arreIns=[]
		arreIns.push("containerIns")
			arreIns.push("iconIns")
			arreIns.push("tituloIns")
			arreIns.push("instalacion")
			
		arreiFC=[]
		arreiFC.push("containerFc")
			arreiFC.push("iconFc")
			arreiFC.push("tituloFc")
			arreiFC.push("facturacion")
		
		seguimientoSitio.removeStyleSeguimientoSitio(arreMC)
		seguimientoSitio.removeStyleSeguimientoSitio(arreAG)
		seguimientoSitio.removeStyleSeguimientoSitio(arreIns)
		seguimientoSitio.removeStyleSeguimientoSitio(arreiFC)
	},
	removeStyleSeguimientoSitio:function(arreglo){
		let container = arreglo[0]
		let icon = arreglo[1]
		let titulo = arreglo[2]
		let domData = arreglo[3]
		
		$("#"+container).addClass('espaciodeSeguimientoSitio');
		if(container == "containerMC"){
			$("#"+container).removeClass('espacioSeguimientoSitioInicial')
		}else{
			$("#"+container).removeClass('espaciodeSeguimientoSitioExpandir')
		}
		
		$("#"+icon).addClass('bgroundDesabilitarSeguimiento');
		$("#"+domData).removeClass('showPOM');
		$("#"+domData).addClass('hidePOM')
		$("#"+titulo).addClass('colorDesabilitarSeguimiento')
		 
	}, 
	validaDataNUll:function(dato){
		if(dato == null || dato == undefined){
			return "--";
		}
		return dato;
		
	}
}
//** FIN SEGUIMIENTO HISTORIAL SITIO COT **//



function handleFileSelect(evt) {
    var files = evt.target.files;
    
    for (var i = 0, f; f = files[i]; i++) {

      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      reader.onload = (function(theFile) {
        return function(e) {
          var span = document.createElement('span');
          span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);

      reader.readAsDataURL(f);
    }
  }





function file(dato){
	var fileInputTextDiv = document.getElementById('file_input_text_div'+dato);
	var fileInput = document.getElementById('file_input_file'+dato);
	var fileInputText = document.getElementById('file_input_text'+dato);
	fileInput.addEventListener('change', changeInputText);
fileInput.addEventListener('change', changeInputText);

	function changeInputText() {
	  var str = fileInput.value;
	  var i;
	  if (str.lastIndexOf('\\')) {
	    i = str.lastIndexOf('\\') + 1;
	  } else if (str.lastIndexOf('/')) {
	    i = str.lastIndexOf('/') + 1;
	  }
	  fileInputText.value = str.slice(i, str.length);
	}

	function changeState() {
	  if (fileInputText.value.length != 0) {
	    if (!fileInputTextDiv.classList.contains("is-focused")) {
	      fileInputTextDiv.classList.add('is-focused');
	    }
	  } else {
	    if (fileInputTextDiv.classList.contains("is-focused")) {
	      fileInputTextDiv.classList.remove('is-focused');
	    }
	  }
	}
}



