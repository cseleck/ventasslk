

function openLoader(mensaje, tipo){
	swal({
		text:mensaje,
		type: tipo,
		showConfirmButton : true
	});
}
var dashboardSF = {
			getCotsVendedor : function() {
				
				$.ajax({
					url : 'getCotsVendedor',
					type : "POST",
					dataType : 'json',
					data : {
					},
					success : function(jsonResponse, textStatus, jqXHR) {
						
						if(jsonResponse.success){
							$.each(jsonResponse.result, function(index, valor) {
								agregarFilaTablaIdCot(valor);
							});
							
							
							    
						}else{
							openLoader(jsonResponse.mensaje,'info')
						}
						
					},complete: function(result){


						swal.close();
					},
					error : function(jqXHR, textStatus, errorThrown) {
						openLoader(jqXHR.statusText,'info')
					}
				});
				
			},
			getDetalleCot:function(idCot){
				$.ajax({
					url : 'getDetalleCot',
					type : "POST",
					dataType: 'json',
					data: {
						'idCot' : idCot
					},
					success: function(result){
						if(result.success){
							informacion.getDataCliente(result);
						}else{
							openLoader(result.mensaje,'info')
						}
					},
					complete: function(result){
						//openLoader(result,'info');
					},
					error : function(jqXHR, textStatus, errorThrown) {
						openLoader(jqXHR.statusText,'info')
					}
				});
			},
			getChatterCot:function(idCot){
				Chatters.ajaxChatter(idCot)
			},
			getSitiosCotizacion:function(idCot){
				$.ajax({
					url : 'getSitiosCot',
					type : "POST",
					dataType: 'json',
					data: {
						'idCot' : idCot
					},
					success: function(result){
						if(result.success){
							SitioCotizacion.tableSitioCot(result)
						}else{
							openLoader(result.mensaje,'info')
						}
						
					},
					complete: function(){
						
					},
					error : function(jqXHR, textStatus, errorThrown) {
						openLoader(jqXHR.statusText,'info')
					}
				});
			},
			getPlanesSitiosCot:function(idSitio, name){
				swal(json_swal);
				setTimeout(function() {
				$.ajax({
					url : 'getPlanesSitiosCot',
					type : "POST",
					dataType: 'json',
					data: {
						'idSitio': idSitio
					},
					success: function(result){
						if(result.success){
							SitioCotizacion.addTableResultDelait(result,name);
						}else{
							openLoader(result.mensaje,'info')
						}	
					},
					complete: function(){
						swal.close();
					},
					error : function(jqXHR, textStatus, errorThrown) {
						openLoader(jqXHR.statusText,'info')
					}
				});	
				},2500);
			},
			getHistorialSitio:function( idOS, idCot){
				if(idOS == undefined || idOS == null){
					idOS=''
				}
				swal(json_swal);
				setTimeout(function() {
				$.ajax({
					url : 'getHistorialSitio',
					type : "POST",
					dataType: 'json',
					data: {
						'idOS' : idOS,
						'idCot': idCot
					},
					success: function(result){
						if(result.success){
							seguimientoSitio.addInfoToSeguimientoSitio(result)
						}else{
							openLoader(result.mensaje,'info')
						}
						
					},
					complete: function(){
						swal.close();
					},
					error : function(jqXHR, textStatus, errorThrown) {
						openLoader(jqXHR.statusText,'info')
					}
				});	},2500);
			},

			insertChatter:function(idparent){
				Chatters.ajaxInsertChatter(idparent)
			},
			guardaChatterCotREs : function (){
				Chatters.ajaxChatterCommentCotRes()
			},
			getFileChatter:function(data,selecType){
				Chatters.ajaxgetFileChatter(data,selecType)
			}, 
			downloadFile:function(base64, nombre, tipo, selectype){	
				Chatters.filterDownloadFile(base64, nombre, tipo, selectype)	
			}
	}
