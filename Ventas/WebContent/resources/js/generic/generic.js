var Chatters={
		ajaxChatter:function(idCot)
		{
			if(idCot == undefined)
			{
				idCot = 'a06Q0000009v04JIAQ'
			}
			
			$.ajax({
				url : 'getChatterCot',
				type : "POST",
				dataType: 'json',
				data: {
					'idCot' : idCot
				},
				success: function(result){
					Chatters.getPOM(result)
				},
				complete: function(){

				},
				error: function(jqXHR, textStatus, errorThrown){
					console.log(jqXHR)
					openLoader(jqXHR.statusText,'info')
					
				}
			});
		},
		
		ajaxInsertChatter:function(idparent){
			var form = $('#formCharter'+idparent)[0];
			var data = new FormData(form);

			swal({html: '<strong>Espera un momento...</strong>',allowOutsideClick: false});
		    swal.showLoading();
			setTimeout(function() {
			$.ajax({
				type : "POST",
				async: false,
				enctype : 'multipart/form-data',
				url : 'insertChartterCot',
				data: data,
				processData : false,
				contentType : false,
				cache : false,
				success: function(result){
					if(result.success){
						dashboardSF.getChatterCot($('#idCotChatter').val());
						$('#formCharter'+idparent)[0].reset();
						$('#formCharterCot2')[0].reset();
					}else{
						//openLoader(result.mensaje, 'info')
					}
				},
				complete: function(){
					swal.close();
				},
				error: function(jqXHR, textStatus, errorThrown){
					swal.close()
					openLoader(jqXHR.statusText,'info')
				}
			});
			}, 2500);
		},
		ajaxChatterCommentCotRes:function(){
			var form = $('#formCharterCot2')[0];
			var data = new FormData(form);
			var idCot = $('#idCot');
			swal({background: '#00000000',showCancelButton: false,showConfirmButton: false,imageUrl: 'resources/img/login/2.gif'});
			setTimeout(function() {
				$.ajax({
					type : "POST",
					async: false,
					enctype : 'multipart/form-data',
					url : 'insertChartterCot',
					data : data,
					processData : false,
					contentType : false,
					cache : false,
					success : function(jsonResponse, textStatus, jqXHR) {
						if (jsonResponse.success) {
							    dashboardSF.getChatterCot($('#idCotChatter').val());
								$('#formCharterCot2')[0].reset();
								$('#formCharterCot2')[0].reset();
						} else {
							//openLoader(jsonResponse.mensaje, 'info')
						}
					},
					complete: function(){
						swal.close();
					},
					error : function(jqXHR, textStatus, errorThrown) {				
						//errorAjax(jqXHR);
						openLoader("Ocurrio un Error, Intenta de nuevo..", 'info')
					}
				});
			}, 2500);
		},
		getPOM:function(result){
			$("#contenedorPOM").html('');
			let conteBody='';
			let conte='';	
			let conteFilePadre=''
				
			$.each(result.result, function( index, value ) {
				conteBody='';
				$.each(value.subComentario, function( indexs, values ) {
					conteBody += Chatters.getChatter(values.name,values.date,values.body, values.idDoc )
				})
				
				if(value.idDoc == null){
					conteFilePadre =''
				}else{
					conteFilePadre = 
					'<div class="mdc-layout-grid__inner">'	
						+'<label class="fileStyle" onclick="Chatters.downloadFile(\''+value.idDoc+'\',0)">'
							+'<i class="fa fa-paperclip styleColorIconFile"></i>'
						+'</label>'
					+'</div>'
				}
				
				conte +=
						'<div class="mdc-layout-grid__inner">'
							+'<div class="mdc-layout-grid__cell--span-12 ">'
									+'<span class = "headerInfo">'+value.name+'</span>'
							+'</div>'
						+'</div>'
						+'<div class="mdc-layout-grid__inner">'
							+'<div class="mdc-layout-grid__cell--span-12">'
								+'<span class="fechaInfo" id="datePadre">'+value.date+'</span>'
							+'</div>'
						+'</div>'
						+'<div class="mdc-layout-grid__inner">'	
							+'<div class="mdc-layout-grid__cell--span-12 seguimientoSitioLetterDetalle parrafosComent">'
								+value.body
							+'</div>'
						+'</div>'
						+conteFilePadre
						+ (conteBody == undefined ? ' ' : conteBody)
						
						+'<div class="mdc-layout-grid__inner">'
							+'<div class="mdc-layout-grid__cell--span-8">'
							+'</div>'
							+'<div class="mdc-layout-grid__cell--span-4">'
								+'<span class="headerInfoEnviar" onclick="Chatters.displayBLockComment(\''+value.id+'\')">-Responder</span>'
							+'</div>'
						+'</div>'
						+'<div class="mdc-layout-grid__inner hidePOM" id="POMTextArea'+value.id+'">'
							+'<div class="mdc-layout-grid__cell--span-12">'
								
								+'<form id="formCharter'+value.id+'" enctype="multipart/form-data" >'
										+'<input type="hidden" name = "nivel" value = "2">'
										+'<input type="hidden" name = "idParent" value = "'+value.id+'">'
										+'<div class="mdc-layout-grid" style="width: 100%; padding: 0px;">'
												+'<textarea name = "textCharter" class="textAreaStyle" placeholder="Escribe un comentario" rows="3" ></textarea>'
												+' <div class="iconFileClip" style="display: flex">'
														+'<div class="">'
															+' <label class="fileStyle" onclick="file(\''+value.id+'\')">'
																+'<i class="fa fa-paperclip styleColorIconFile"></i>'
																+' <input id="file_input_file'+value.id+'" name="file"  accept="*" class="none" type="file" />'
															+' </label>'
														+'</div>'
													+'<div id="file_input_text_div'+value.id+'" style="width: 100%;">'
														+'<input class="styleFilePaterno" style="width: 100%;"  type="text" disabled readonly id="file_input_text'+value.id+'" />'
														+' <label class="mdl-textfield__label" for="file_input_text'+value.id+'"></label>'
													+' </div>'
												+' </div>'
										+'</div>'
								+'</form>'
								
								+'<div class="mdc-layout-grid__inner">'
									+'<div class="mdc-layout-grid__cell--span-8">'
									+'</div>'
									+'<div class="mdc-layout-grid__cell--span-4">'
										+'<span class="headerInfoEnviar"  style=" font-size: 16px;" onclick="dashboardSF.insertChatter(\''+value.id+'\')">Enviar</span>'
										+'</div>'
								+'</div>'
								
							+'</div>'
						+'</div>';
			})
				$("#contenedorPOM").html(conte);
			
		},
		getChatter:function(name, fecha, texto, id){
			let conteFilePadre=''
				
				if(id !=null){
					conteFilePadre= 
					'<div class="mdc-layout-grid__inner">'	
						+'<div class="mdc-layout-grid__cell--span-1">'+'</div>'
							+'<div class="mdc-layout-grid__cell--span-11">'	
								+'<label class="fileStyle" onclick="Chatters.downloadFile(\''+id+'\',0)">'
									+'<i class="fa fa-paperclip styleColorIconFile"></i>'
								+'</label>'
							+'</div>'
						+'</div>'
					+'</div>'
					
						
				}
			let POMSecondCommen = 
					'<div class="mdc-layout-grid__inner">'
						+'<div class="mdc-layout-grid__cell--span-1">'
						+'</div>'
						+'<div class="mdc-layout-grid__cell--span-11">'
								+'<span id="namePadre" class = "headerInfo">'+name+'</span>'
						+'</div>'
					+'</div>'
					+'<div class="mdc-layout-grid__inner">'
						+'<div class="mdc-layout-grid__cell--span-1">'
						+'</div>'
						+'<div class="mdc-layout-grid__cell--span-11">'
							+'<span class="fechaInfo" id="datePadre">'+fecha+'</span>'
						+'</div>'
					+'</div>'
					+'<div class="mdc-layout-grid__inner">'	
						+'<div class="mdc-layout-grid__cell--span-1">'
						+'</div>'
						+'<div class="mdc-layout-grid__cell--span-11 seguimientoSitioLetterDetalle">'
							+ (texto == undefined ? ' ' : texto)
						+'</div>'
					+'</div>'
					+conteFilePadre;

			return POMSecondCommen
		}, 
		addComment:function(dato, id){
			let comment = $("#txtComment"+dato).val()
			let idCommen = dato;
			dashboardSF.insertChatter("", idCommen, comment, 2)
			
		},
		displayBLockComment:function(data){
			$("#POMTextArea"+data).removeClass('hidePOM')
			$("#POMTextArea"+data).addClass('showPOM')
		}, 
		downloadFile:function(dato, selecType){
			dashboardSF.getFileChatter(dato,selecType)
		},
		ajaxgetFileChatter:function(data, selectType){
			swal({background: '#00000000',showCancelButton: false,showConfirmButton: false,imageUrl: 'resources/img/login/2.gif'});
			setTimeout(function() {
				$.ajax({
					url : 'getFileContentVersion',
					type : "POST",
					dataType: 'json',
					data: {
						'idFile' : ''+data
					},
					success: function(result){
						dashboardSF.downloadFile(result.result.dataFile,result.result.title, result.result.fileExtension, selectType )
					},
					complete: function(){
						swal.close()
					},
					error: function(){
						
					}
				});
				}, 2500);
		},
		filterDownloadFile:function(base64, nombre, tipo, selectType){
			switch (tipo) {
			case "pdf":
				base64 = 'data:application/pdf;base64,'+base64
				Chatters.filterSelect(base64,nombre,"application/pdf",selectType)
				break;
			case "png":
				base64 = 'data:image/png;base64,'+base64
				if(selectType == 0){
					download(base64,nombre,"image/png")
				}else{
					Chatters.filterSelect(base64,nombre,"image/png",selectType)
				}
				
				break;
			case "jpg":
				base64 = 'data:image/jpeg;base64,'+base64
				if(selectType == 0){
					download(base64,nombre,"image/jpeg")
				}else{
					Chatters.filterSelect(base64,nombre,"image/jpeg",selectType)
				}
				break;
			case "doc":
				base64 = 'data:application/msword;base64,'+base64
				download(base64,nombre,"application/msword")
				break;
			case "xlm":
				base64 = 'data:application/vnd.ms-excel;base64,'+base64
				download(base64,nombre,"application/vnd.ms-excel")
				break;
			default:
				break;
			}
		},
		filterSelect:function(base64, nombre, tipo, selectType){
			switch (selectType) {
			case 0:
				//base64 = 'data:application/pdf;base64,'+base64
				download(base64,nombre,"application/pdf")
				break;
			case 1:
				//base64 = 'data:image/png;base64,'+base64
				doctosCliente.showDocto(base64,selectType, tipo)
				//download(base64,nombre,"image/png")
				break;
			case 2:
				//base64 = 'data:image/jpeg;base64,'+base64
				doctosCliente.showDocto(base64,selectType, tipo)
				//download(base64,nombre,"image/jpeg")
				break;

			}
		}
	}

