$(document).ready(function(){
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
	    xhr.setRequestHeader(header, token);
	});

	dashboardMC.getListDocsCot("a06Q0000009v04JIAQ")
})


var plantilla_cotizacion="";

var detalleOport={
		
		getDataCliente:function(result){
			
			// gral
			let folioCot  = detalleOport.validaDato(result.result.folioCot)
			let nombreCot = detalleOport.validaDato(result.result.nombreCot)
				
			// Account
			let nombreCta      = detalleOport.validaDato(result.result.nombreOpor)
			let razonSocCta    = detalleOport.validaDato(result.result.razonSocCta)
			let tipoPersonaCta = detalleOport.validaDato(result.result.tipoPersonaCta)
			let telCta         = detalleOport.validaDato(result.result.telCta)
			let sectorCta      = detalleOport.validaDato(result.result.sectorCta)
			let contactoCta    = detalleOport.validaDato(result.result.contactoCta)
				
			// Oportunidad__r
			let nombreOpor     = detalleOport.validaDato(result.result.nombreOpor)
			let NumeroOpor     = detalleOport.validaDato(result.result.NumeroOpor)
			let tipoOpor       = detalleOport.validaDato(result.result.tipoOpor)
			let subTipoOpor    = detalleOport.validaDato(result.result.subTipoOpor)
			let origenOpor     = detalleOport.validaDato(result.result.origenOpor)
			let segmentoOpor   = detalleOport.validaDato(result.result.segmentoOpor)
			let etapaOpor      = detalleOport.validaDato(result.result.etapaOpor)
			let probaOpor      = detalleOport.validaDato(result.result.probaOpor)
			
			$("#folioCot").text(folioCot)
			$("#headerSeguiSItio").text(folioCot)
			$("#nombreCot").text(nombreCot)	
			
			$("#nombreCta").text(nombreCta)
			$("#razonSocCta").text(razonSocCta)
			$("#tipoPersonaCta").text(tipoPersonaCta)
			$("#telCta").text(telCta)
			$("#sectorCta").text(sectorCta)
			$("#contactoCta").text(contactoCta)
			
			$("#nombreOpor").text(nombreOpor)
			$("#NumeroOpor").text(NumeroOpor)
			$("#tipoOpor").text(tipoOpor)
			$("#subTipoOpor").text(subTipoOpor)
			$("#origenOpor").text(origenOpor)
			$("#segmentoOpor").text(segmentoOpor)
			$("#etapaOpor").text(etapaOpor)
			$("#probaOpor").text(probaOpor+'%')
		}, 
		validaDato:function(data){
			try {
					if(data ==undefined){
						return '---';
					}else{
						return data;
					}
				}
				catch(error) {
					return '---';
				}
			
		},
		
	}

var doctosCliente={	
		flagLeftImg:true,
		flagRightImg:true,
		selectValida: $("#selectValida").change(function(){
			let idSelectValida= $(this).val()
			if(idSelectValida == 1){
				$("#seccionSelectRechazo").removeClass("showPOM")
				$("#seccionSelectRechazo").addClass("hidePOM")
				
				$("#seccionInputfolioCredito").removeClass("hidePOM")
				$("#seccionInputfolioCredito").addClass("showPOM")
			}else{
				
				$("#seccionInputfolioCredito").removeClass("showPOM")
				$("#seccionInputfolioCredito").addClass("hidePOM")
				
				$("#seccionSelectRechazo").removeClass("hidePOM")
				$("#seccionSelectRechazo").addClass("showPOM")
			}
		}),
	/*	addDoctosinView:function(arreglo){
			let template = doctosCliente.templateDoctos(arreglo)
			$("#contentDoctos").html('')
			$("#contentDoctos").html(template)
			
		}, */
		changeSelect1:$("#select1").change(function(){
			
			$("#embedDocto").removeClass('showPOM')
			$("#embedDocto").addClass('hidePOM')

			$("#imgDocto").removeClass('showPOM')
			$("#imgDocto").addClass('hidePOM')
			
			$("#imgDocto").attr('src', '')
			$("#embedDocto").attr('src', '')
			
			let id =        $(this).find(':selected').data('id')
			let nombre =    $(this).find(':selected').data('nombre')
			let extencion = $(this).find(':selected').data('tipo')
			
			Chatters.ajaxgetFileChatter(id,1)
		}),
		changeSelect2:$("#select2").change(function(){
			
			$("#embedDocto2").removeClass('showPOM')
			$("#embedDocto2").addClass('hidePOM')

			$("#imgDocto2").removeClass('showPOM')
			$("#imgDocto2").addClass('hidePOM')
			
			$("#imgDocto2").attr('src', '')
			$("#embedDocto2").attr('src', '')
			
			let id =        $(this).find(':selected').data('id')
			let nombre =    $(this).find(':selected').data('nombre')
			let extencion = $(this).find(':selected').data('tipo')
			
			Chatters.ajaxgetFileChatter(id,2)
		}),
		showDocto:function(data, select,archivo){
			archivo = archivo.split("/")
			doctos=data;
			if(select==1){
				if(archivo[0] == "image" ){
					$("#imgDocto").attr('src', doctos)
					$("#imgDocto").removeClass('hidePOM').addClass('showPOM')
				}else{
					$("#embedDocto").attr('src', doctos)
					$("#embedDocto").removeClass('hidePOM').addClass('showPOM')
				}
			}else{
				
				if(archivo[0] == "image" ){
					$("#imgDocto2").attr('src', doctos)
					$("#imgDocto2").removeClass('hidePOM').addClass('showPOM')
				}else{
					$("#embedDocto2").attr('src', doctos)
					$("#embedDocto2").removeClass('hidePOM').addClass('showPOM')
				}
			}
		}, 
		cargaSelectDoctos:function(data){
			$("#select1").html('')
			$("#select2").html('')
			    let option=''
				let option2=''
					
				option  = '<option value="" selected>Selecciona una Opci&oacute;n</option>';
				option2 = '<option value="" selected>Selecciona una Opci&oacute;n</option>';
			 $.each(data, function( index, value ) {
				 
				 option  += '<option data-id="'+value.id+'" data-nombre="'+value.nombre+'" data-tipo="'+value.extencion+'" ">'+value.nombre+'</option>';
				 option2 += '<option data-id="'+value.id+'" data-nombre="'+value.nombre+'" data-tipo="'+value.extencion+'" ">'+value.nombre+'</option>';

			 });
			$("#select1").html(option)
			$("#select2").html(option2)
			
		},
		showZoomIzquierdo:function(){
			if(this.flagLeftImg){
				this.flagLeftImg = false;
				$("#imgDocto").addClass('ladoIzqueirdoImg')
			}else{
				this.flagLeftImg = true;
				$("#imgDocto").removeClass('ladoIzqueirdoImg')
			}
			
		},
		showZoomDerecho:function(){
			
			if(this.flagRightImg){
				$("#imgDocto2").addClass('ladoDerechoImg')
				this.flagRightImg = false;
			}else{
				$("#imgDocto2").removeClass('ladoDerechoImg')
				this.flagRightImg = true;
			}
		}
		/*templateDoctos:function(arreglo){
			
			let contenido = '';
			let count=0;
			$.each(arreglo, function (i, datos) {
			nombre = datos.name
			id = datos.id
			if(count == 0){
	contenido =''+
			'<div class="row container" >'+
				' <div class="col-4" onclick="showImagenDoctoCliente(\''+id+'\',\''+nombre+'\' )">'+
					''+doctosCliente.validaTipoArchivo(datos.tipo)+''+
					'<div>'+
               		    '<span class="colorLetterGris">'+datos.name+'</span>'+
               		 '</div>'+
               	 '</div>';
				}
				
				if(count == 1){
	contenido +='<div class="col-4" onclick="showImagenDoctoCliente(\''+id+'\',\''+nombre+'\')">'+
					''+doctosCliente.validaTipoArchivo(datos.tipo)+''+
               	 	'<div>'+
               	 		'<span class="colorLetterGris">'+nombre+'</span>'+
               	 	'</div>'+
               	 '</div>'
				}
				
				
				if(count == 2){
	contenido +='<div class="col-4" onclick="showImagenDoctoCliente(\''+id+'\',\''+nombre+'\')">'+
					  ''+doctosCliente.validaTipoArchivo(datos.tipo)+''+
                      '<div>'+
                       		'<span class="colorLetterGris">'+nombre+'</span>'+
                      '</div>'+
                '</div>'
					count=0;
				}
				count++;
		 	});
			
			contenido += '</div>'
			return contenido;
		}, 
		
		validaTipoArchivo:function(tipo){
			let archivo=''
			switch (tipo) {
			case "imagen":
				archivo = '<img src="resources/img/dashboardMA/imagen.png" style="width: 5em;">'
				break;
			case "pdf":
				archivo = '<img src="resources/img/dashboardMA/pdf.jpg" style="width: 5em;">'
				break;
			case "":
				break;
			case "excel":
				archivo = '<img src="resources/img/dashboardMA/excel.jpg" style="width: 5em;">'
				break;
				
			default:
				break;
			}	
			
			return archivo;
		}*/

}



function showImagenDoctoCliente(){
	$("#imgDocto").attr('src', '')
	$("#embedDocto").attr('src', '')
	
	$("#imgDocto2").attr('src', '')
	$("#embedDocto2").attr('src', '')
	swal({background: '#00000000', showCancelButton: false,showConfirmButton: false,imageUrl: 'resources/img/login/2.gif'});
	$("#modalShowImage").modal('show')
	setTimeout(function(){swal.close()}, 1000);
	
}

var menu= {
		primerasTen:$(".m_menu").click(function(){
			dashboardMC.getUltimasCotsMC()
		 }),
		 getShowUltimasCotsMC:function(json_cotizaciones){
			 $("#menu").show();
			 $("#listacotizaciones").html("");
      	     var htmlcot="";
			 $.each(json_cotizaciones, function( index, value ) {
				  htmlcot+="<li onclick='menu.callInfoUserFull(\""+value.idCot+"\")' > <div class='light b_blue light_trigger '  style='background: "+value.semaforo+"'></div><div class='info' onclick='menu.listar(\""+value.folioCot+"\")'><p>"+value.folioCot+"</p>"
		                   +"<p class='company tam14'>"+value.nombreCot+";</p> </div></li>";
			});
			 $("#listacotizaciones").html(htmlcot);
		 },
		 listar:function(numcotiza){
			/* $.each(json_empresas, function( index, value ) {
				 console.log(index);
				 console.log(value.id+ " "+numcotiza);
				 if(value.id === numcotiza){
					 console.log("entrado...");
					$("#lbl_name").html(value.razonsocial);
					$("#lbl_rpt").html(value.status);
					$("#lbl_oportunidad").html(value.razonsocial);
				 }
			 });*/
			//console.log(numcotiza);
		 },
		 tablaCot:$("#menu_show_more1").on('click', function(){
			 dashboardMC.getListCotsMC()
		 }), 
		 
		 showTable:function(json_cotizaciones){
			 try{
				 $('#idListaCot').DataTable().destroy();
			 }catch(error){
				 
			 }
			 $("#idListaCotBody").html('') 
				
				let contenido =''
				 $.each(json_cotizaciones, function( index, value ) {
					 let idCot = value.idCot
					 let folio = value.folioCot
					 let razon = value.razonSocial
					 let nombreCot = value.nombreCot
					 let oportunidad = value.oportunidad
					 contenido +=''
					+'<tr onclick="menu.callInfoUserFull(\''+idCot+'\')">'
						+'<td>'
							+'<div class="row">'
								+'<div class="col-1">'
									+'<div class="light b_blue" style="margin-top: 1em; background:'+value.semaforo+'">'
									+'</div>'
							    +'</div>'
							    +'<div class="col">'
							    	+'<span>'+folio+'</span>'
							    	+'<p class="company">'+razon+'</p>'
							   +'</div>'
							+'</div>'
						+'</td>'
						+'<td>'+nombreCot+'</td>'
						+'<td>'+oportunidad+'</td>'
						+'<td>--</td>'
					+'</tr>';
				});
			 
			 $("#idListaCotBody").html(contenido); 
			 $('#idListaCot').DataTable({
			    	"processing" : true,
					"language" : idioma_espanol_not_font
				});

		 },
		 callInfoUserFull:function(data){
			    swal({background: '#00000000',showCancelButton: false,showConfirmButton: false,imageUrl: 'resources/img/login/2.gif'});
				dashboardSF.getChatterCot(data);
				dashboardMC.getDetalleCot(data);
				dashboardMC.getListDocsCot(data);
				setTimeout(function(){swal.close()}, 1000);
				$("i.mdc-icon-button").click();
		 }	 
}

 
$(".close_m_menu").click(function(){$("#menu").hide()});
$(".tooltip_blue_trigger").hover(function(){
	var a=$(this).offset().top-$(window).scrollTop()-10;
	$(".tooltip_blue").css("top",a+"px").show()
    },function(){$(".tooltip_blue").hide()}
);

$("#menu_show_more1").click(function(){
	$("#menu").hide();
	$("#menu1").show();
	
});
$("#menu1 .close_m i").click(function(){
	$("#menu").css("width","20%");
	$("#menu1").hide();
});