var dashboardMC={
		
		getDetalleCot:function(idCot){
			if(idCot == undefined){
				idCot = 'a06Q0000009v04JIAQ'
			}
			
			$.ajax({
				url : 'getDetalleCot',
				type : "POST",
				dataType: 'json',
				data: {
					'idCot' : idCot
				},
				success: function(result){
					
					if(result.success){
						detalleOport.getDataCliente(result);
					}else{
						console.log("result",result.mensaje);
					}
					
				},
				complete: function(result){
					console.log("result",result);
				},
				error: function(result){
					console.log("result",result);
				}
			});
		},
		getUltimasCotsMC:function(){
			let arre =[]
			$.ajax({
				url : 'getUltimasCotsMC',
				type : "POST",
				dataType: 'json',
				data: {},
				success: function(result){
					if(result.success){
						menu.getShowUltimasCotsMC(result.result)
					}else{
						console.log("result",result.mensaje);
					}
				},
				complete: function(result){
					console.log("result",result);
				},
				error: function(result){
					console.log("result",result);
				}
			});
		},
		getListCotsMC:function(){
			let arre =[]
			$.ajax({
				url : 'getListCotsMC',
				type : "POST",
				dataType: 'json',
				data: {},
				success: function(result){
					if(result.success){
						menu.showTable(result.result)
					}else{
						console.log("result",result.mensaje);
					}
				},
				complete: function(result){
					console.log("result",result);
				},
				error: function(result){
					console.log("result",result);
				}
			});
		},
		getListDocsCot:function(data){
			$.ajax({
				url : 'getListDocsCot',
				type : "POST",
				dataType: 'json',
				data: {
					"idCot":data
				},
				success: function(result){
					if(result.success){
						doctosCliente.cargaSelectDoctos(result.result)
					}else{
						console.log("result",result.mensaje);
					}
				},
				complete: function(result){
					
				},
				error: function(result){
					console.log("result",result);
				}
			});
		}
}


var dashboardSF = {
		getChatterCot:function(idCot){
			if(idCot == undefined){
				idCot = 'a06Q0000009v04JIAQ'
			}
			Chatters.ajaxChatter(idCot)
		},
		insertChatter:function(idparent){
			if(idparent == undefined){
				idparent = 'a06Q0000009v04JIAQ'
					$("#idCotChatter").val(idparent)
			}
			Chatters.ajaxInsertChatter(idparent)
			swal({background: '#00000000',showCancelButton: false,showConfirmButton: false,imageUrl: 'resources/img/login/2.gif'});
		},
		guardaChatterCotREs : function (){
			idparent = 'a06Q0000009v04JIAQ'
			$("#idCotChatter").val(idparent)
			swal({background: '#00000000',showCancelButton: false,showConfirmButton: false,imageUrl: 'resources/img/login/2.gif'});
			Chatters.ajaxChatterCommentCotRes()
		},
		getFileChatter:function(data, selecType){
			Chatters.ajaxgetFileChatter(data,selecType)
		}, 
		downloadFile:function(base64, nombre, tipo,selectType){	
			Chatters.filterDownloadFile(base64, nombre, tipo,selectType)	
		}
}

var idioma_espanol_not_font = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontrar\u00F3n resultados",
	    "sEmptyTable":     "Ning\u00Fan dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    }
	};