<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="icon" type="image/png" href="<c:url value="/resources/img/favicon.png"/>">
        <link href="<c:url value="/resources/libs/material/css/material-components-web.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/login/login.css"/>" rel="stylesheet" >

<title>Login</title>

</head>
   <body>
   <br><br>
        <div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
                <div id="block" class="mdc-layout-grid__cell--span-5-tablet mdc-layout-grid__cell--span-7-desktop">
                    <div class="header">
                        <img alt="logo Totalplay Empresarial" src="<c:url value="/resources/img/login/logo_tp.png"/>">
                        <p><span>Bienvenido</span><br>de nuevo</p>
                    </div>
                    <form action="<c:url value="perform_login" />" id="formLogin" method="post" class="login-form login" autocomplete="off">
                        <div class="group_input">
                            <label for="user">Usuario</label>
                           
                            <input type="text" placeholder="Usuario" required autocomplete="off" id="user_user" name="user" />
                        </div>
                        <div class="group_input">
                            <label for="password">Contrase&ntilde;a</label>
                            <input type="password" placeholder="Contrase&ntilde;a" required autocomplete="off" id="user_pswd"  name="pass"/>
                        </div>
                        <div class="mdc-form-field group_check">
                            <div class="mdc-checkbox">
                                <input type="checkbox"
                                       class="mdc-checkbox__native-control"
                                       id="checkbox-1"/>
                                <div class="mdc-checkbox__background">
                                    <svg class="mdc-checkbox__checkmark"
                                         viewBox="0 0 24 24">
                                        <path class="mdc-checkbox__checkmark-path"
                                              fill="none"
                                              d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                    </svg>
                                    <div class="mdc-checkbox__mixedmark"></div>
                                </div>
                            </div>
                            <label for="checkbox-1">Recordarme</label>
                        </div>
                        <div>
                            <a>Problemas con la cuenta</a>
                            <a>Olvid&oacute; la contrase&ntilde;a</a>
                        </div>
                        <div>
	                        <button type="submit" class="mdc-button mdc-button--raised">Acceder</button>
				  			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="<c:url value="/resources/libs/material/js/material-components-web.min.js"/>"></script>
        <script src="<c:url value="/resources/libs/jquery/jquery.min.js" />"></script>
        <script src="<c:url value="/resources/js/login/login.js" />"></script>
<!--         <script src="js/final.min.js"></script> -->
    </body>
</html>
