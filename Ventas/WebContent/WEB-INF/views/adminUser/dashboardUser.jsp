<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    
  
        <link rel="stylesheet" href="<c:url value="/resources/libs/material/css/material-components-web.min.css"/>">
        <link rel="stylesheet"href="<c:url value="/resources/libs/bootstrap/css/bootstrap.min.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/resources/libs/fontawesome/css/all.min.css" ></c:url>">
        <link rel="stylesheet" href="<c:url value="/resources/css/generic/generic.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/resources/css/dashboardMA/dashboardMA.css"></c:url>">
    </head>
    </head>
    <body>
	     <jsp:include page="../tools/header.jsp" />
        
        <div class="mdc-layout-grid content_general content_gral">
            <div class="mdc-layout-grid__inner">
				
            	<div class="mdc-layout-grid__cell--span-12 block mdc-elevation--z2">
                    <div class="header">
                        <p>Alta Usuario</p>
                    </div>
                    <div class="body">
                    <div id="mdc-section">
					<!-- data-mdc-auto-init="MDCTextField" -->	
					


		
	





<!-- 
<div id="bridgeSizeSelect" class="mdc-select demo-width-class mdc-select--outlined demo-enhanced-width demo-enhanced-select" data-mdc-auto-init="MDCSelect">
 
  <i class="mdc-select__dropdown-icon" ></i>
  <div class="mdc-select__selected-text">Ventas</div>
  <div class="mdc-select__menu demo-width-class mdc-menu mdc-menu-surface" >
    <ul class="mdc-list" id="my-list">
 
     
      <li class="mdc-list-item mdc-list-item--selected" aria-selected="true" role="option" data-value="1">
        Ventas
      </li>
      <li class="mdc-list-item"  role="option" data-value="2">
        Mesa de Control
      </li>
    </ul>
  </div>
  <span class="mdc-floating-label mdc-floating-label--float-above">Tipo de Perfil</span>
  <div class="mdc-line-ripple"></div>
</div>
 -->

 

    
 
 
 
 
 <br>
 
 <div class="mdc_layout_grid__cell">
    <div style="display: flex;">

   
<div class="mdc-text-field mdc-text-field--outlined mdc-text-field--with-leading-icon" data-mdc-auto-init="MDCTextField">
		  <i class="material-icons mdc-text-field__icon" tabindex="0" role="button"><i class="fas fa-user"></i></i>
		  <input type="text" id="my-input" class="mdc-text-field__input">
		  <div class="mdc-notched-outline">
		    <div class="mdc-notched-outline__leading"></div>
		    <div class="mdc-notched-outline__notch">
		      <label for="my-input" class="mdc-floating-label">Nombre</label>
		    </div>
		    <div class="mdc-notched-outline__trailing"></div>
		  </div>
		</div>
  
    
    <div class="mdc-text-field mdc-text-field--outlined mdc-text-field--with-leading-icon" data-mdc-auto-init="MDCTextField" >
		  <i class="material-icons mdc-text-field__icon" tabindex="0" role="button"><i class="fas fa-archive"></i></i>
		  <input type="text" id="my-input" class="mdc-text-field__input">
		  <div class="mdc-notched-outline">
		    <div class="mdc-notched-outline__leading"></div>
		    <div class="mdc-notched-outline__notch">
		      <label for="my-input" class="mdc-floating-label">N&uacute;mero de Empleado</label>
		    </div>
		    <div class="mdc-notched-outline__trailing"></div>
		  </div>
		</div>

   

    <div class="mdc-select" data-mdc-auto-init="MDCSelect">
      <select id="select-input" class="mdc-select__native-control">
     
        
        <option value="2">
          Mesa de control
        </option>
        <option value="1">
          Ventas
        </option>
      </select>
      <label for="select-input" class="mdc-floating-label">Tipo de perfil</label>
      <div class="mdc-line-ripple"></div>
    </div>
    </div>
    </div>
    <br>
    <div class="mdc-button">
        <button type="submit" class="mdc-button mdc-button--raised">Crear usuario</button>
    </div>
 
 
  
  

</div>
                     </div>
	            </div>
	            </div>
	            </div>
	
	          
                              
		<script src="<c:url  value="/resources/libs/jquery/jquery.min.js"/>" type="text/javascript"></script>
		<script src="<c:url  value="/resources/libs/bootstrap/js/popper.min.js"/>" type="text/javascript"></script>
		<script src="<c:url  value="/resources/libs/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
        <script src="<c:url  value="/resources/libs/material/js/material-components-web.min.js"/>"></script>
        <script src="<c:url  value="/resources/js/dashboardMA/dashboardMA.js"/>" type="text/javascript"></script>
         <script src="<c:url  value="/resources/js/dashboardMesa/dashboardMC.js"/>"></script>
        <script src="<c:url  value="/resources/libs/downloadFiles/downloadFile.js"/>" type="text/javascript"></script>
        <script>window.mdc.autoInit();
        
   
        
        </script>
    </body>
</html>
