 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
 <link rel="stylesheet" href="<c:url value="/resources/css/dashboardMesa/mesacontrol.css"/>" >
 
  <div id="menu" class="mdc-elevation--z6 menu_left" style="display: none">
	    	<div class="tooltip_blue">
                <span class="arrow"></span>
                <p><span class="light b_red"></span>6 d�as o m�s</p>
                <p><span class="light b_orange"></span>3 a 5 d�as</p>
                <p><span class="light b_green_light"></span>1 a 2 d�as</p>
             </div>
	         <div class="logox">
	             <div class="imagex">
	                 <img src="<c:url  value="/resources/img/dashboardSF/logo1.svg"/>" class="imglogo">
	                 <i class="material-icons close_m_menu">clear</i>
	             </div>
	         </div>
	         <div class="title_section">
                <p class="tam14">Cotizaciones por revisar</p>
             </div>
             <ul id="listacotizaciones"></ul>
             
             <div class="footer">
                <a class="link "><i class="material-icons tooltip_blue_trigger">
                    help
                </i>Sem�foro de tiempo</a>
                <a class="btn_m" id="menu_show_more1">Cotizaciones revisadas</a>
            </div>
	     </div>
	     <!-- ============================================== 1111 ===================== -->
	     <div id="menu1" class="mdc-elevation--z6 otro" style="width: 90%;display: none">
            <div class="tooltip_blue" style="top: 571.95px; display: none;">
                <span class="arrow"></span>
                <p><span class="light b_red"></span>6 d�as o m�s</p>
                <p><span class="light b_orange"></span>3 a 5 d�as</p>
                <p><span class="light b_green_light"></span>1 a 2 d�as</p>
            </div>
            <div class="logo">
	             <div class="image" style="width: 30%;">
	                 <img src="<c:url  value="/resources/img/dashboardSF/logo1.svg"/>">
	             </div>
	             <div class="collapse" style="display: block;">
                    <!--input class="finder" type="text" placeholder="Buscar cotizaci�n">
                    <i class="material-icons">search</i -->
                    <div class="close_m" style="float: right;">
                        <i class="mdc-icon-button material-icons">
                            clear
                        </i>
                    </div>
                </div>
            </div>
            <table id="idListaCot" class="table table-striped table-bordered" style="width:100%">
            <thead>
           		 <tr>
	            	<th>Cotizaciones pendientes</th>
	            	<th>Fecha de registro</th>
	            	<th>Tipo de cotizaci�n</th>
	            	<th>Renta mensual sin impuestos</th>
           		 </tr>
            </thead>
	        <tbody id="idListaCotBody">
	        	<tr>
	        		<td>
	        		 <div class="row">
					    <div class="col-1">
		        		<div class="light b_blue" style="margin-top: 1em;">
		        		</div>
					    </div>
					    <div class="col">
					     <span>COT092683</span>
		                <p class="company">Liverpool S.A. de C.V.</p>
					    </div>
					  </div>
	        		</td>
	        		<td>como</td>
	        		<td>hacer</td>
	        		<td>duncitonar</td>
	        	</tr>
	        </tbody>
	        </table>
        </div>
	       
 <header class="mdc-top-app-bar mdc-elevation--z3 full_screen">
	      	<div class="mdc-top-app-bar__row">
                <section class="mdc-top-app-bar__section">
                    <div class="mdc-layout-grid__inner">
                        <div class="mdc-layout-grid__cell--span-3">
                            <i class="material-icons m_menu">menu</i>
                            <img src="<c:url value="/resources/img/dashboardSF/logo_color.png"/>" class="logo">
                        </div>
                        
                        <div class="mdc-layout-grid__cell--span-6"></div>
                        <div class="mdc-layout-grid__cell--span-2">
                        
                            <div id="account">
                                <p><span>Mar�a Delia</span> - Ventas </p>
                            </div>
                        </div>
                        
                        <div class="mdc-layout-grid__cell--span-1 divisor_v">
                            <a class="mdc-icon-button material-icons settings">settings</a>
                        </div>
                    </div>
                   
                    <div id="menu_settings" class="mdc-elevation--z2">
                        <div class="block_arrow_up"></div>
                        <ul>
                            <li><a href="index.html">Cerrar sesi�n</a></li>
                        </ul>
                    </div>
                </section>
            </div>
  </header>
