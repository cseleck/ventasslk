<div class="mdc-layout-grid " style="margin: 0px;">
	<div>
		<div class="mdc-layout-grid__inner">
			<div class="mdc-layout-grid__cell--span-1">
				<span  data-toggle="tooltip" title="Regresar" onclick= "returnTable();" class="styloBackTable"><i class="fas fa-chevron-left" ></i></span>				
			</div>
			<div class="mdc-layout-grid__cell--span-5 ">
				<span class="headerInfo">Cotizaci&oacute;n:</span>&nbsp;
				<span class="colorLetterGris" id="folioCot"></span>
			</div>
			<div class="mdc-layout-grid__cell--span-5 ">
				<span class="headerInfo">Nombre:</span>
				<span class="colorLetterGris" id="nombreCot" ></span>
			</div>
			<div class="mdc-layout-grid__cell--span-1">
			</div>
			
		</div>
	</div>
	 <div class="divisor_h" style="margin: 6px;"></div>
 	<div class=" mdc-layout-grid__cell--span-12">
   	  <p class = "headerInfo" style="margin-block-end: 5px;">Cuenta</p>
	</div>
   	<div class="mdc-layout-grid__inner">
  		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Nombre:</span>
		  		<span class="colorLetterGris" id="nombreCta" ></span>
		  	</div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Sector:</span>
		  		<span class="colorLetterGris" id="sectorCta"></span>
		  	</div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Razon Social:</span>
		  		<span class="colorLetterGris" id="razonSocCta"></span>
		  	</div>
		</div>
		<div class="mdc-layout-grid__inner">
  		    <div class="mdc-layout-grid__cell--span-4">
  		    	<span class="headerInfo" >Tipo Persona:</span>
  		    	<span class="colorLetterGris" id="tipoPersonaCta"></span>
  		    </div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Telefono:</span>
		  		<span class="colorLetterGris" id="telCta"></span>
		  	</div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Contacto Princiapal:</span>
		  		<span class="colorLetterGris" id="contactoCta"></span>
		  	</div>
		</div>
	<div class="divisor_h" style="margin: 6px;"></div>
	<div class=" mdc-layout-grid__cell--span-12">
   	 <p class = "headerInfo" style="margin-block-end: 5px;">Oportunidad</p>
	</div>
   	<div class="mdc-layout-grid__inner">
   			<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Nomnbre:</span>
		  		<span class="colorLetterGris" id="nombreOpor"></span>
		  	</div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Numero:</span>
		  		<span class="colorLetterGris" id="NumeroOpor"></span>
		  	</div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Tipo:</span>
		  		<span class="colorLetterGris" id="tipoOpor"></span>
		  	</div>
		</div>
		<div class="mdc-layout-grid__inner">
     		<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">SubTipo:</span>
		  		<span class="colorLetterGris" id="subTipoOpor"></span>
		  	</div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Origen:</span>
		  		<span class="colorLetterGris" id="origenOpor"></span>
		  	</div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Etapa:</span>
		  		<span class="colorLetterGris" id="etapaOpor"></span>
		  	</div>		  
		</div>
		<div class="mdc-layout-grid__inner">
  			<div class="mdc-layout-grid__cell--span-8">
		  		<span class="headerInfo">Segmento:</span>
		  		<span class="colorLetterGris" id="segmentoOpor"></span>
		  	</div>
		  	<div class="mdc-layout-grid__cell--span-4">
		  		<span class="headerInfo">Probabilidad:</span>
		  		<span class="colorLetterGris" id="probaOpor"></span>
		  	</div>		  		  	
		</div>
</div>
