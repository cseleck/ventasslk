<span id = "headerSeguiSItio" class="headerInfo" style="padding: 5px;"></span>
<br>
<div class="row cardSeguimientoSitio">
	<ul>
		<li>
			<div id="containerMC" class="mdc-layout-grid espaciodeSeguimientoSitio">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell--span-2 colorLetterGris">
						<div class="color">
							<p id="iconMC" class="b_green bgroundDesabilitarSeguimiento colorLetterGris" style="margin: 5px;">MC</p>
						</div>
					</div>
					<div class="mdc-layout-grid__cell--span-8 colorLetterGris">
						<p id="tituloMC" class="SeguimientoSitioLetter colorDesabilitarSeguimiento">Mesa de Control</p>
					</div>
				</div>
				<div class="hidePOM" id="mesacontrol">
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1 "></div>
						<div class="mdc-layout-grid__cell--span-6 ">
							<span id="spanMC" class="seguimientoSitioLetterDetalle">En Validaci&oacute;n</span>
						</div>
					</div>
				</div>
			</div>
		</li>

		<li>
			<div id="containerAgen" class="mdc-layout-grid espaciodeSeguimientoSitio">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell--span-2 colorLetterGris">
						<div class="color">
							<p id="iconAgen" class="b_blue bgroundDesabilitarSeguimiento colorLetterGris" style="margin: 5px;">AG</p>
						</div>
					</div>
					<div class="mdc-layout-grid__cell--span-8 colorLetterGris">
						<div>
							<p id="tituloAgen" class="SeguimientoSitioLetter colorDesabilitarSeguimiento">Agendamiento</p>
						</div>
					</div>
				</div>
				<div class="hidePOM" id="agendamiento">
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1 "></div>
						<div class="mdc-layout-grid__cell--span-11 ">
							<span class="headerInfo">Fecha:</span>
							<span id="spanAGFecha" class="colorLetterGris seguimientoSitioLetterDetalle">12/11/2019</span>
						</div>
					</div>
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1 "></div>
						<div class="mdc-layout-grid__cell--span-11">
							<span class="headerInfo">Turno:</span>
							<span id="spanAGTurno" class="colorLetterGris seguimientoSitioLetterDetalle">Vespertino</span>
						</div>
					</div>
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1"></div>
						<div class="mdc-layout-grid__cell--span-11">
							<span class="headerInfo">Orden de Servicio:</span>
							<span id="spanAGOS" class="colorLetterGris seguimientoSitioLetterDetalle">OS-123</span>
						</div>
					</div>
				</div>
			</div>
		</li>
		
		<li>
			<div id="containerIns" class="mdc-layout-grid espaciodeSeguimientoSitio">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell--span-2 colorLetterGris">
						<div class="color">
							<p id="iconIns" class="b_green bgroundDesabilitarSeguimiento" style="margin: 5px;">IN</p>
						</div>
					</div>
					<div class="mdc-layout-grid__cell--span-8 colorLetterGris">
						<div>
							<p id="tituloIns" class="SeguimientoSitioLetter colorDesabilitarSeguimiento">Instalaci&oacute;n</p>
						</div>
					</div>
				</div>
				<div class="hidePOM" id="instalacion">
					<div class="mdc-layout-grid__inner ">
						<div class="mdc-layout-grid__cell--span-1 "></div>
						<div class="mdc-layout-grid__cell--span-11 ">
							<span class="headerInfo">T&eacute;cnico:</span>
							<span id="spanInsTecnico" class="colorLetterGris seguimientoSitioLetterDetalle">12/11/2019</span>
						</div>
					</div>
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1 "></div>
						<div class="mdc-layout-grid__cell--span-11 colorLetterGris">
							<span class="headerInfo">Auto:</span>
							<span id="spanInsAuto" class="colorLetterGris seguimientoSitioLetterDetalle">Spark : MAB2251</span>
						</div>
					</div>
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1"></div>
						<div class="mdc-layout-grid__cell--span-11 colorLetterGris">
							<span class="headerInfo">Estatus:</span>
							<span id="spanInsStatus" class="colorLetterGris seguimientoSitioLetterDetalle">Pendiente</span>
						</div>
					</div>
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1"></div>
						<div class="mdc-layout-grid__cell--span-11 colorLetterGris">
							<span class="headerInfo">SubEstatus:</span>
							<span id="spanInsSubStatus" class="colorLetterGris seguimientoSitioLetterDetalle">En Trabajo</span>
						</div>
					</div>
				</div>
			</div>
		</li>
		
		
		<li style="opacity: 1">
			<div id="containerFc" class="mdc-layout-grid espaciodeSeguimientoSitio">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell--span-2 colorLetterGris">
						<div class="color">
							<p id="iconFc" class="b_red bgroundDesabilitarSeguimiento colorLetterGris" style="margin: 5px;">FC</p>
						</div>
					</div>
					<div class="mdc-layout-grid__cell--span-8 colorLetterGris">
						<div>
							<p id="tituloFc" class="SeguimientoSitioLetter colorDesabilitarSeguimiento">Facturaci&oacute;n</p>
						</div>
					</div>
				</div>
				<div class="hidePOM" id="facturacion">
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1 "></div>
						<div class="mdc-layout-grid__cell--span-11 ">
							<span class="headerInfo">Fecha:</span>
							<span id="spanFacFecha" class="colorLetterGris">12/11/2019</span>
						</div>
					</div>
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-1 "></div>
						<div class="mdc-layout-grid__cell--span-11">
							<span class="headerInfo">Cuenta Factura:</span>
							<span id="spanFacTurno" class="colorLetterGris">CTA01010101</span>
						</div>
					</div>
				</div>
			</div>
		</li>
		
<!-- 		<li> -->
<!-- 			<div id="containerFc" class="mdc-layout-grid espaciodeSeguimientoSitio"> -->
<!-- 				<div class="mdc-layout-grid__inner"> -->
<!-- 					<div class="mdc-layout-grid__cell--span-2 colorLetterGris"> -->
<!-- 						<div class="color"> -->
<!-- 							<p id="iconFc" class="b_red bgroundDesabilitarSeguimiento">FC</p> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="mdc-layout-grid__cell--span-8 colorLetterGris"> -->
<!-- 						<div> -->
<!-- 							<p id="tituloFc" -->
<!-- 								class="SeguimientoSitioLetter colorDesabilitarSeguimiento">Facturaci&oacute;n</p> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div id="facturacion" class="hidePOM"> -->
<!-- 					<div class="mdc-layout-grid__inner"> -->
<!-- 						<div class="mdc-layout-grid__cell--span-1 "></div> -->
<!-- 						<div class="mdc-layout-grid__cell--span-11 "> -->
<!-- 							<span class="headerInfo">Fecha de Facturaci&oacute;n:</span> -->
<!-- 							<span id="spanFCfecha" class="colorLetterGris">12/11/2019</span> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="mdc-layout-grid__inner"> -->
<!-- 						<div class="mdc-layout-grid__cell--span-1 "></div> -->
<!-- 						<div class="mdc-layout-grid__cell--span-11 "> -->
<!-- 							<span class="headerInfo">Cuenta Factura:</span> -->
<!-- 							<span id="spanFCcta" class="colorLetterGris">CTA04040440</span> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</li> -->
	</ul>

</div>