<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    		<link rel="icon" type="image/png" href="<c:url value="/resources/img/favicon.png"/>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="_csrf" content="${_csrf.token}"/>
			<meta name="_csrf_header" content="${_csrf.headerName}"/>
            <title>VentasTP</title>
            
            
		
		 <link rel="stylesheet" href="<c:url value="/resources/libs/material/css/material-components-web.min.css"/>">
 		 <link href="<c:url value="/resources/libs/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet">
         <link href="<c:url value="resources/libs/fontawesome/css/all.min.css" />" rel="stylesheet">
         <link href="<c:url value="resources/libs/datatable/css/buttons.dataTables.min.css" />" rel="stylesheet">
         <link href="<c:url value="resources/libs/datatable/css/buttons.bootstrap.min.css"/>" rel="stylesheet">
         <link href="<c:url value="/resources/libs/datatable/css/jquery.dataTables.min.css"/>" rel="stylesheet">
         <link href="<c:url value="resources/libs/datatable/css/responsive.bootstrap.min.css"/>" rel="stylesheet">
         <link href="<c:url value="/resources/libs/sweetalert/css/sweetalert2.min.css"/>" rel="stylesheet">
          <link rel="stylesheet" href="<c:url value="/resources/css/dashboardMesa/mesacontrol.css"/>" >
         <link rel="stylesheet" href="<c:url value="/resources/css/generic/generic.css"></c:url>">
         <link rel="stylesheet" href="<c:url value="/resources/css/menu/menu.css"/>" >
         <link rel="stylesheet" href="<c:url value="/resources/css/dashboardMesa/resume.min.css"/>" >
         <link rel="stylesheet" href="<c:url value="/resources/css/dashboardSF/dashboardSF.css"/>" >
 
    </head>
    <body >
    
        <div id="divTablaCots" style="width: 100%;">
          	<jsp:include page="tablaCotizacion.jsp" />
        </div>
        
        <div id="divDetalleCot" style="width: 100%; display: none;">
          	<jsp:include page="contenedor.jsp" />
        </div>
        
          <script type="text/javascript" src="<c:url value="/resources/libs/jquery/jquery.min.js"></c:url>"></script>
		  <script type="text/javascript" src="<c:url value="/resources/libs/bootstrap/js/popper.min.js"></c:url>"></script>
		  <script type="text/javascript" src="<c:url value="/resources/libs/bootstrap/js/bootstrap.min.js"></c:url>"></script>
		  <script type="text/javascript" src="<c:url value="/resources/libs/datatable/js/jquery.dataTables.min.js"></c:url>"></script>
		  <script type="text/javascript" src="<c:url value="/resources/libs/datatable/js/dataTables.responsive.min.js"></c:url>"></script>
		  <script type="text/javascript" src="<c:url value="/resources/libs/sweetalert/js/sweetalert2.min.js"></c:url>"></script>
		  <script src="<c:url  value="/resources/libs/material/js/material-components-web.min.js"/>"></script>
		   <script type="text/javascript" src="<c:url value="/resources/libs/downloadFiles/downloadFile.js"></c:url>"></script>
           <script type="text/javascript" src="<c:url value="/resources/js/generic/generic.js"></c:url>"></script>
          <script type="text/javascript" src="<c:url value="/resources/js/dashboardSF/dashboardSF.js"></c:url>"></script>
          <script type="text/javascript" src="<c:url value="/resources/js/dashboardSF/ajaxResources.js"></c:url>"></script>
     				 
    </body>
</html>
