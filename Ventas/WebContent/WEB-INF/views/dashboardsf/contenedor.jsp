        <div class="mdc-layout-grid content_generales content_gral headerInit" style="padding: 0px;">
            <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell--span-1 block " style="padding: 15px; background-color: transparent;">
            </div>
            	<div class="mdc-layout-grid__cell--span-6 block " style="padding: 15px; background-color: transparent;">
                
                    <div class="body card" style="padding: 15px;" >
                       <jsp:include page="detalleInformacion.jsp" />
                    </div>
                    <br>
                    <div class="body">
                    	<jsp:include page="sitioCotizacion.jsp" />
                    </div>
                </div>
                
                <div class="mdc-layout-grid__cell--span-4 block "  style="padding: 15px;  padding-left: 0px; background-color: transparent;"> 
                    <div class="body card" style="padding: 15px;">
                    	<jsp:include page="SeguimientoSitio.jsp" />
                    </div>
					<br>
					<div class="body card">
					 	<jsp:include page="../generic/comentarios.jsp" />
					</div>
                </div> 
                <div class="mdc-layout-grid__cell--span-1 block " style="padding: 15px; background-color: transparent;">
            </div>
	        </div>
	    </div>