
<div class="card-body" style="padding: 7px;">
	<div class="comment">
		<div class="content">
			<form id="formCharterCot2" method="post" enctype="multipart/form-data">
				<input type="hidden" id="idCotChatter" name="idParent" value=""> 
				<input type="hidden" name="nivel" value="1"> 
				<span id="divComentarioCot"></span>
				<div class="mdc-layout-grid" style="padding: 0px;">
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell--span-12 ">
							<textarea name="textCharter" class="textAreaStyle" placeholder="Escribe un comentario" rows="4"></textarea>
						</div>
					</div>
				</div>
				<div class="mdc-layout-grid__cell--span-12 ">
					<div class="iconFileClip" style="display: flex">
						<div class="">
							<label class="fileStyle" onclick="file(1)"> 
								<i class="fa fa-paperclip styleColorIconFile"></i> 
								<input id="file_input_file1" name="file" class="none" type="file" />
							</label>
						</div>
						<div id="file_input_text_div1" style="width: 100%;">
							<input class="styleFilePaterno" style="width: 100%;" type="text" disabled readonly id="file_input_text1" /> 
							<label class="mdl-textfield__label" for="file_input_text1"></label>
						</div>
					</div>
				</div>
			</form>
			<div class="mdc-layout-grid" style="padding: 0px;">
				<div class="mdc-layout-grid__inner" >
					<div class="mdc-layout-grid__cell--span-9 backgroundComment"></div>
					<div class="mdc-layout-grid__cell--span-3 ">
						<span class="headerInfoEnviar" style="font-size: 16px;" onclick="dashboardSF.guardaChatterCotREs();">Enviar</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="divisor_h" style="margin: 6px;"></div>
	<div class="scroll-1">
		<div class="">
			<div class="content" id="contenedorPOM"></div>
		</div>
	</div>

</div>
