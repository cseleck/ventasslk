<div class="modal" id="modalShowImage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="" role="document">
        <div class="modal-content" style="height: 98vh;">
            <div class="modal-body p-4">
	         <div class="mdc-layout-grid">
	            <div class="mdc-layout-grid__inner">
	            	 <div class="mdc-layout-grid__cell--span-1"></div>
	            	 <div class="mdc-layout-grid__cell--span-4">
	            	 
	            	<div class="input-group">
					  <select class="custom-select" id="select1">
					    <option selected>Selecciona...</option>
					  </select>
					</div>
	            	 </div>
	            	  <div class="mdc-layout-grid__cell--span-1"></div>
	            	 <div class="mdc-layout-grid__cell--span-4">
	            	 
	            	 <div class="input-group">
					  <select class="custom-select" id="select2" aria-label="Example select with button addon">
					    <option selected>Choose...</option>
					  </select>
					</div>
	            	 </div>
	             </div>
	          </div>
	          
	          <div class="mdc-layout-grid">
	            <div class="mdc-layout-grid__inner">
	            	 <div class="mdc-layout-grid__cell--span-6">
	            	 	<embed id="embedDocto" src="" class="stylePDF hidePOM " style="width: 100%; height: 63vh;">
	            	 	<img class="stylePDF hidePOM" onclick="doctosCliente.showZoomIzquierdo();" id="imgDocto" alt="sin Imagen" src="">
	            	 </div>
	            	
	            	  <div class="mdc-layout-grid__cell--span-6">
	            	  	<div class="mdc-layout-grid__cell--span-6">
	            	 	  <embed id="embedDocto2" src="" class="stylePDF hidePOM " style="width: 100%; height: 63vh;">
	            	 	  <img class="stylePDF hidePOM" onclick="doctosCliente.showZoomDerecho();" id="imgDocto2" alt="sin Imagen" src="">
	            	   </div>
	                 </div>
	             </div>
	          </div>
            </div>
            <div class="modal-footeres">
            	<div class="mdc-layout-grid" style="margin-right: none;     padding: 0px;">
	            	<div class="mdc-layout-grid__inner">
	            		 <div class="mdc-layout-grid__cell--span-2">
	            		 	<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="caratula2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="caratula2">Car&aacute;tula</label>
  								</div>
	            		 </div>
	            		 <div class="mdc-layout-grid__cell--span-2">
	            		 	<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="contrato2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="contrato2">Contrato/Adjunci&oacute;n &oacute; fallo</label>
  								</div>
	            		 </div>
	            		 <div class="mdc-layout-grid__cell--span-2">
	            		 	 <div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="clausulado2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="clausulado2">Clausulado</label>
  								</div>
	            		 </div>
	            		  <div class="mdc-layout-grid__cell--span-2">
	            		 		<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="rfc2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="rfc2">RFC</label>
  								</div>
	            		 </div>
	            		  <div class="mdc-layout-grid__cell--span-2">
	            		 		<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="credito2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="credito2">Bur&oacute; de Cr&eacute;dito</label>
  								</div>
	            		 </div>
	            		  <div class="mdc-layout-grid__cell--span-2">
	            		 		
  									<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="acta2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="acta2">Acta constitutiva</label>
  								</div>
	            		 </div>
	            		 
	            		  
	           		 </div>
	           		 <div class="mdc-layout-grid__inner">
	           			 <div class="mdc-layout-grid__cell--span-2">
	           			 	<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="domicilio2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="domicilio2">Comprobante de Domicilio</label>
  								</div>
	           			 </div>
	           			 <div class="mdc-layout-grid__cell--span-2">
	           			 	<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="ine2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="ine2">Identificaci&oacute;n oficial</label>
  								</div>
	           			</div> 
	           			 <div class="mdc-layout-grid__cell--span-2">
	           			 	<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="con_vista2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="con_vista2">Consentimiento de Vista</label>
  								</div>
	           			</div>
	           			 <div class="mdc-layout-grid__cell--span-2">
	           			 	<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="try_buy2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="try_buy2">Try and buy</label>
  								</div>
	           			</div>
	           			 <div class="mdc-layout-grid__cell--span-2">
	           			 	<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="edocta2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="edocta2">Estado de cuenta</label>
  								</div>
	           			</div>
	           			 <div class="mdc-layout-grid__cell--span-2">
	           			 	<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="validacion2" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="validacion2">Validaci&oacute;n Mesa</label>
  								</div>
	           			</div>
	           		</div> 
	           </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>


