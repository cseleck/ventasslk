<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
   	 	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    	<meta name="_csrf" content="${_csrf.token}"/>
		<meta name="_csrf_header" content="${_csrf.headerName}"/>
      
        <link rel="stylesheet" href="<c:url value="/resources/libs/material/css/material-components-web.min.css"/>">
        <link rel="stylesheet"href="<c:url value="/resources/libs/bootstrap/css/bootstrap.min.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/resources/libs/fontawesome/css/all.min.css" ></c:url>">
        <link rel="stylesheet" href="<c:url value="/resources/css/generic/generic.css"></c:url>">
         <link rel="stylesheet" href="<c:url value="/resources/css/menu/menu.css"/>" >
        <link rel="stylesheet" href="<c:url value="/resources/css/dashboardMesa/dashboardMA.css"></c:url>">
        <link href="<c:url value="/resources/libs/sweetalert/css/sweetalert2.min.css"/>" rel="stylesheet">
    </head>
    </head>
    <body>
	     <jsp:include page="../tools/header.jsp" />
         <div class="mdc-layout-grid content_general content_gral">
            <div class="mdc-layout-grid__inner">
             <div class="mdc-layout-grid__cell--span-1 colorTextArea"></div>
            	 <div class="mdc-layout-grid__cell--span-7">
                 	<div class="block mdc-elevation--z2">
                    <div class="header">
                        <p>Detalle de la oportunidad</p>
                    </div>
                    <div class="body">
						<jsp:include page="detalledeOportunidad.jsp" />
                    </div>
                </div>
					<br/>
					<div class="block mdc-elevation--z2">
					  <div class="">
                             <div>
                      			 <p class="styleheader">Chartter</p>
                   			 </div>
							<div class="body">
					 			<jsp:include page="../generic/comentarios.jsp" />
							</div>
					  </div>
					</div>
                </div> 
                <div class ="mdc-layout-grid__cell--span-3 block mdc-elevation--z2">
                 <div class="">
                    		<jsp:include page="checksDoctoClientes.jsp" />
					</div>
                </div>
            </div>
        </div>         
		<script src="<c:url  value="/resources/libs/jquery/jquery.min.js"/>" type="text/javascript"></script>
		 		        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
         <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		<script src="<c:url  value="/resources/libs/bootstrap/js/popper.min.js"/>" type="text/javascript"></script>
		<script src="<c:url  value="/resources/libs/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
        <script src="<c:url  value="/resources/libs/material/js/material-components-web.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/libs/sweetalert/js/sweetalert2.min.js"></c:url>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/js/generic/generic.js"></c:url>"></script>
         <script src="<c:url  value="/resources/js/dashboardMesa/ajaxResourceMC.js"/>"></script>
         <script src="<c:url  value="/resources/js/dashboardMesa/dashboardMC.js"/>"></script>
        <script src="<c:url  value="/resources/libs/downloadFiles/downloadFile.js"/>" type="text/javascript"></script>
        
    </body>
</html>
</html>