<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <body>
    				<div class="" >
    				<div>
                        <p class="styleheader">Documentaci&oacute;n del Cliente</p>
                    </div>
                    <div style="float: right;">
                    	<label  data-toggle="tooltip" data-placement="top" onclick="showImagenDoctoCliente()"><i class="fa fa-eye" style="cursor: pointer;"></i></label>
                    </div>
					  <div class="card-body">
					  <!--  div class="custom-control custom-checkbox">
 							<input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
							<label class="custom-control-label" for="customCheck">Seleccionar todo</label>
						
  					  </div-->
					  	<div class="row">
							<div class="col">
    							<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="caratula" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="caratula">Car&aacute;tula</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="contrato" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="contrato">Contrato/Adjunci&oacute;n &oacute; fallo</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="clausulado" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="clausulado">Clausulado</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="rfc" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="rfc">RFC</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="credito" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="credito">Bur&oacute; de Cr&eacute;dito</label>
  								</div>
  									<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="acta" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="acta">Acta constitutiva</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="domicilio" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="domicilio">Comprobante de Domicilio</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="ine" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="ine">Identificaci&oacute;n oficial</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="con_vista" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="con_vista">Consentimiento de Vista</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="try_buy" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="try_buy">Try and buy</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="edocta" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="edocta">Estado de cuenta</label>
  								</div>
  								<div class="col-12 custom-control custom-checkbox">
   									 <input type="checkbox" class="custom-control-input" id="validacion" name="example1">
    								 <label class="custom-control-label colorLetterGris" for="validacion">Validaci&oacute;n Mesa</label>
  								</div>
							</div>
					      </div>
					      <div class="row">
					      	<div class="col-6">
					      		  <select class="custom-select colorLetterGris" id="selectValida" name="selectValida">
								    <option selected>Selecciona...</option>
								    <option value="1">Validada</option>
								    <option value="2">Rechazada</option>
								    <option value="3">Pendiente</option>
								 </select>
					      	</div>
					      	<div class="col-6">
					      		<div id="seccionInputfolioCredito" class="hidePOM">
		  						    	<label class="colorLetterGris colorLetterGris">Folio de Credito</label>
		  								<input class="form-control form-control-sm" type="text" placeholder="">
		  							</div>
		    						
		    						<div id="seccionSelectRechazo" class="hidePOM">			
										<select class="custom-select colorLetterGris" id="inputGroupSelect01">
										    <option selected>Selecciona...</option>
										    <option value="1">Se fue</option>
										    <option value="2">Trabajo de mas</option>
										    <option value="3">ni idea</option>
										  </select>
			 						</div>
					      	</div>
		 				 </div>
					      <div class="form-group">
     						 <label class="colorLetterGris" for="comment">Escribe un comentario:</label>
    						 <textarea class="form-control colorTextArea" rows="3" id="comment" name="text"></textarea>
   						 </div>
					      
					     <div class="row">
					     	<div class="col-md-1"></div>
					     	<div class="col-md-6" style="font-size: 1.5em;    text-align: center;"> <button type="button" class="btn styleButtonDoctoCliente">Enviar</button></div>
					     </div>
					   </div>
					</div>
					
    </body>
 		<jsp:include page="modals/showImage.jsp" />